import {configure} from '@storybook/react';
import {setOptions} from '@storybook/addon-options';
import '../src';

setOptions({
  name: 'Rocket',
  goFullScreen: false,
  downPanelInRight: true,
  sortStoriesByKind: false,
  hierarchySeparator: /\/|\./,
  showDownPanel: true,
});

const req = require.context('../src', true, /\.story\.tsx$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}


configure(loadStories, module);