var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { action, computed, observe } from 'mobx';
import './style.css';
import { observer } from 'mobx-react';
import { Hero, Container, Title, Subtitle, Card, CardImage, CardFooter, CardFooterItem } from 'bloomer';
import * as Consumption from './steps/Consumption/Consumption';
import * as Usage from './steps/Usage/Usage';
import * as Results from './steps/Results/Results';
// import * as Stepper from '../../modules/Stepper';
import { types as t } from 'mobx-state-tree';
import * as Stepper from '../Stepper';
import classNames from 'classnames';
var localStore = {
    stepper: new Map(),
};
export var Store = t
    .model('Calculator', {
    title: t.string,
    subtitle: t.string,
    height: t.optional(t.string, 'auto'),
    consumption: Consumption.Store,
    usage: Usage.Store,
    results: Results.Store,
    activeStep: t.string,
    co2Emissions: t.number,
})
    .views(function (self) {
    return {
        get backButtonVisible() {
            var stepper = localStore.stepper.get(self);
            return self.activeStep !== stepper.steps[0].id;
        },
        get nextButtonVisible() {
            var stepper = localStore.stepper.get(self);
            var steps = stepper.steps;
            return self.activeStep !== steps[steps.length - 1].id;
        },
        get stepper() {
            return localStore.stepper.get(self);
        },
        get activeStepValid() {
            switch (self.activeStep) {
                case 'consumption': return self.consumption.valid;
                default: return true;
            }
        },
        get summary() {
            return self.usage.summary;
        },
        get prettySummary() {
            var current = self.usage.summary.currentLightsWithExistingControls;
            var existing = self.usage.summary.newLightsWithExistingControls;
            var proposed = self.usage.summary.newLightsWithProposedAdditionalControls;
            return {
                currentLightsWithExistingControls: {
                    maxLoad: parseFloat(current.maxLoad.toFixed(1)),
                    hoursPerAnnum: parseInt(current.hoursPerAnnum.toFixed(0)),
                    runningHours: parseInt(current.runningHours.toFixed(0)),
                    consumption: parseInt(current.consumption.toFixed(0)),
                    electricityCost: parseInt(current.electricityCost.toFixed(0)),
                    maintenanceCost: parseInt(current.maintenanceCost.toFixed(0)),
                    totalRunningCost: parseInt(current.totalRunningCost.toFixed(0)),
                },
                newLightsWithExistingControls: {
                    maxLoad: parseFloat(existing.maxLoad.toFixed(1)),
                    hoursPerAnnum: parseInt(existing.hoursPerAnnum.toFixed(0)),
                    runningHours: parseInt(existing.runningHours.toFixed(0)),
                    consumption: parseInt(existing.consumption.toFixed(0)),
                    electricityCost: parseInt(existing.electricityCost.toFixed(0)),
                    reduction: parseInt(existing.reduction.toFixed(0)),
                    percentageOfUsage: parseInt(new Number(existing.percentageOfUsage * 100).toFixed(0)),
                    electricityCostReduction: parseInt(existing.electricityCostReduction.toFixed(0)),
                    maintenanceCostReduction: parseInt(existing.maintenanceCostReduction.toFixed(0)),
                    maintenanceCost: parseInt(existing.maintenanceCost.toFixed(0)),
                    totalSaving: parseInt(existing.totalSaving.toFixed(0)),
                    costOfUpgrade: parseInt(existing.totalSaving.toFixed(0)),
                    paybackTime: parseFloat(existing.paybackTime.toFixed(1)),
                    greenBusiness: existing.greenBusiness,
                    greenBusinessContribution: parseInt(existing.greenBusinessContribution.toFixed(0)),
                    greenBusinessReducedUpgradeCost: parseInt(existing.greenBusinessReducedUpgradeCost.toFixed(0)),
                    greenBusinessPaybackTime: parseFloat(existing.greenBusinessPaybackTime.toFixed(1)),
                    co2Reduction: parseFloat(existing.co2Reduction.toFixed(2)),
                },
                newLightsWithProposedAdditionalControls: {
                    maxLoad: parseFloat(proposed.maxLoad.toFixed(1)),
                    hoursPerAnnum: parseInt(proposed.hoursPerAnnum.toFixed(0)),
                    runningHours: parseInt(proposed.runningHours.toFixed(0)),
                    consumption: parseInt(proposed.consumption.toFixed(0)),
                    electricityCost: parseInt(proposed.electricityCost.toFixed(0)),
                    reduction: parseInt(proposed.reduction.toFixed(0)),
                    percentageOfUsage: parseInt(new Number(proposed.percentageOfUsage * 100).toFixed(0)),
                    electricityCostReduction: parseInt(proposed.electricityCostReduction.toFixed(0)),
                    maintenanceCostReduction: parseInt(proposed.maintenanceCostReduction.toFixed(0)),
                    maintenanceCost: parseInt(proposed.maintenanceCost.toFixed(0)),
                    totalSaving: parseInt(proposed.totalSaving.toFixed(0)),
                    costOfUpgrade: parseInt(proposed.totalSaving.toFixed(0)),
                    paybackTime: parseFloat(proposed.paybackTime.toFixed(1)),
                    greenBusiness: proposed.greenBusiness,
                    greenBusinessContribution: parseInt(proposed.greenBusinessContribution.toFixed(0)),
                    greenBusinessReducedUpgradeCost: parseInt(proposed.greenBusinessReducedUpgradeCost.toFixed(0)),
                    greenBusinessPaybackTime: parseFloat(proposed.greenBusinessPaybackTime.toFixed(1)),
                    co2Reduction: parseFloat(proposed.co2Reduction.toFixed(2)),
                },
            };
        }
    };
})
    .actions(function (self) {
    function setActiveStep(activeStep) {
        var stepper = localStore.stepper.get(self);
        stepper.setActiveStep(activeStep);
        self.activeStep = activeStep;
    }
    return { setActiveStep: setActiveStep };
})
    .actions(function (self) {
    function updateUsageByConsumption(fields) {
        var newAverageElectricityPriceField = fields.find(function (a) { return a.id === 'averageElectricityPrice'; }) || { value: '' };
        var newOccupancyHoursField = fields.find(function (a) { return a.id === 'occupancyHours'; }) || { value: '' };
        var greenBusinessField = fields.find(function (a) { return a.id === 'greenBusiness'; }) || { value: false };
        self.usage.setElectricityPrice(parseInt(newAverageElectricityPriceField.value, 10));
        self.usage.setOccupancyHours(parseInt(newOccupancyHoursField.value, 10));
        self.usage.setGreenBusiness(greenBusinessField.value);
        self.usage.setCo2Emissions(self.co2Emissions);
    }
    function nextStep() {
        var stepper = localStore.stepper.get(self);
        stepper.nextStep();
    }
    function previousStep() {
        var stepper = localStore.stepper.get(self);
        stepper.previousStep();
    }
    function afterCreate() {
        var activeStep = self.activeStep;
        var stepper = Stepper.Store.create({
            steps: [
                { id: 'consumption', label: self.consumption.label, active: activeStep === 'consumption', disabled: true },
                { id: 'usage', label: self.usage.label, active: activeStep === 'usage', disabled: true },
                { id: 'results', label: self.results.label, active: activeStep === 'results', disabled: true },
            ]
        });
        observe(stepper, 'activeStepId', function (change) { return self.setActiveStep(change.newValue); });
        localStore.stepper.set(self, stepper);
        // lets sync consumpstion data with usage
        updateUsageByConsumption(self.consumption.data);
        observe(self.consumption, 'data', function (_a) {
            var newValue = _a.newValue;
            updateUsageByConsumption(newValue);
        }, true);
    }
    return { afterCreate: afterCreate, nextStep: nextStep, previousStep: previousStep };
});
var Calculator = (function (_super) {
    __extends(Calculator, _super);
    function Calculator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Calculator.prototype.nextStep = function () {
        this.props.store.nextStep();
    };
    Calculator.prototype.previousStep = function () {
        this.props.store.previousStep();
    };
    Object.defineProperty(Calculator.prototype, "stepper", {
        get: function () {
            console.log(this.props.store);
            return (React.createElement(Stepper.Stepper, { store: this.props.store.stepper }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Calculator.prototype, "content", {
        get: function () {
            var _a = this.props.store, consumption = _a.consumption, activeStep = _a.activeStep, usage = _a.usage;
            switch (activeStep) {
                case 'consumption': return React.createElement(Consumption.Consumption, { store: consumption });
                case 'usage': return React.createElement(Usage.Usage, { store: usage });
                default: return React.createElement(Results.Results, { calculatorStore: this.props.store });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Calculator.prototype, "backButton", {
        get: function () {
            return this.props.store.backButtonVisible
                ? React.createElement(CardFooterItem, { tag: "a", onClick: this.previousStep }, "Back")
                : null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Calculator.prototype, "nextButton", {
        get: function () {
            var _a = this.props.store, nextButtonVisible = _a.nextButtonVisible, activeStepValid = _a.activeStepValid;
            if (!nextButtonVisible)
                return null;
            var className = classNames({ 'button': !activeStepValid });
            var props = {
                className: className,
                tag: activeStepValid ? 'a' : 'p',
                disabled: activeStepValid ? undefined : true,
                onClick: activeStepValid ? this.nextStep : undefined
            };
            return React.createElement(CardFooterItem, __assign({}, props), "Next");
        },
        enumerable: true,
        configurable: true
    });
    Calculator.prototype.render = function () {
        var _a = this.props.store, title = _a.title, subtitle = _a.subtitle, height = _a.height;
        return (React.createElement("div", { className: "rocket", style: { height: height } },
            React.createElement(Card, { style: { flex: 1, height: 'inherit' } },
                React.createElement(CardImage, null,
                    React.createElement(Hero, { isColor: "primary" },
                        React.createElement("div", { className: "hero-body" },
                            React.createElement(Container, null,
                                React.createElement(Title, { isSize: 4 }, title),
                                React.createElement(Subtitle, null, subtitle))))),
                this.stepper,
                this.content,
                React.createElement(CardFooter, { style: { position: 'absolute', bottom: 0, width: '100%' } },
                    this.backButton,
                    this.nextButton))));
    };
    __decorate([
        action.bound
    ], Calculator.prototype, "nextStep", null);
    __decorate([
        action.bound
    ], Calculator.prototype, "previousStep", null);
    __decorate([
        computed
    ], Calculator.prototype, "stepper", null);
    __decorate([
        computed
    ], Calculator.prototype, "content", null);
    __decorate([
        computed
    ], Calculator.prototype, "backButton", null);
    __decorate([
        computed
    ], Calculator.prototype, "nextButton", null);
    Calculator = __decorate([
        observer
    ], Calculator);
    return Calculator;
}(React.Component));
export { Calculator };
//# sourceMappingURL=Calculator.js.map