var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as storybook from '@storybook/react';
import * as React from 'react';
import * as Calculator from './Calculator';
import { observer } from 'mobx-react';
var stories = storybook.storiesOf('Led calculator', module);
var StoryWrapper = (function (_super) {
    __extends(StoryWrapper, _super);
    function StoryWrapper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StoryWrapper.prototype.render = function () {
        return React.createElement("div", { style: { padding: '12px' } }, this.props.children);
    };
    StoryWrapper = __decorate([
        observer
    ], StoryWrapper);
    return StoryWrapper;
}(React.Component));
function createSnapshoot() {
    return {
        title: 'Energy LED Payback Calculator',
        subtitle: 'Powering change',
        height: '1000px',
        consumption: {
            label: 'Consumption',
            fields: [
                {
                    type: 'input',
                    id: 'averageElectricityPrice',
                    label: 'Average electricity price',
                    placeholder: '10',
                    suffix: 'p/kWh',
                    mandatory: true,
                    value: '',
                    format: 'number',
                },
                {
                    type: 'input',
                    id: 'occupancyHours',
                    label: 'Occupancy hours per annum',
                    placeholder: '3210',
                    suffix: 'hrs/annum',
                    mandatory: true,
                    format: 'number',
                    value: '',
                    helpText: 'Occupancy Hours = Hours per day x Days per week x Weeks per year.' +
                        'So for an office open 6 days a week, 8am - 6pm, 52 weeks a year, the calculation is 6 x 10 x 52 = 3,120.',
                },
                {
                    type: 'checkbox',
                    id: 'lightingControls',
                    label: 'Do you have lighting controls?',
                    helpText: 'Controls typically include motion sensors, daylight harvesting, dimmer switches, ' +
                        'centralised lighting control systems. Click here for more information.',
                    value: false,
                    mandatory: false,
                },
                {
                    type: 'input',
                    id: 'expectedReduction',
                    placeholder: '10',
                    format: 'number',
                    suffix: '%',
                    mandatory: true,
                    label: 'Expected reduction in usage from the controls',
                    showIfChecked: ['lightingControls'],
                    value: '',
                },
                {
                    type: 'checkbox',
                    id: 'greenBusiness',
                    markdown: true,
                    label: 'Eligible for Green Business Funding',
                    helpText: 'Small and medium businesses, schools, charities can obtain a non-repayable 15% ' +
                        'capital contribution, up to £5,000. Open to Small Medium Enterprises (SME), maintained schools and some' +
                        'others. [Click here to download](www.google.co.uk) more information, ' +
                        'or email [info@spiritenergy.co.uk](mailto:info@spiritenergy.co.uk) or check your availability.',
                    value: false,
                    mandatory: false,
                }
            ]
        },
        usage: {
            label: 'Usage',
            controls: [
                {
                    id: 'none',
                    label: 'None',
                    costPerFitting: 0,
                    reduction: 0,
                },
                {
                    id: 'dimming',
                    label: 'Dimming',
                    costPerFitting: 30,
                    reduction: 15,
                },
                {
                    id: 'absenceDetection',
                    label: 'Absense detection',
                    costPerFitting: 50,
                    reduction: 30,
                },
                {
                    id: 'presenceDetection',
                    label: 'Presence detection',
                    costPerFitting: 50,
                    reduction: 30,
                },
                {
                    id: 'daylightHarvesting',
                    label: 'DaylightHarvesting',
                    costPerFitting: 50,
                    reduction: 25,
                },
                {
                    id: 'timedSwitching',
                    label: 'Timed switching',
                    costPerFitting: 20,
                    reduction: 15,
                }
            ],
            lights: [],
            availableLights: lights,
        },
        results: {
            label: 'Results'
        },
        activeStep: 'consumption'
    };
}
var Story = (function (_super) {
    __extends(Story, _super);
    function Story() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Story.prototype.render = function () {
        return (React.createElement(StoryWrapper, null,
            React.createElement(Calculator.Calculator, { store: this.props.store })));
    };
    Story = __decorate([
        observer
    ], Story);
    return Story;
}(React.Component));
stories.add('consuption step', function () {
    var snapshoot = createSnapshoot();
    var store = Calculator.Store.create(snapshoot);
    return React.createElement(Story, { store: store });
});
stories.add('usage step', function () {
    var snapshoot = createSnapshoot();
    snapshoot.consumption.fields.forEach(function (field) {
        if (!field.mandatory)
            return;
        var value;
        switch (field.type) {
            case 'input':
                value = field.format === 'string' ? 'Jhon Wick' : '1234';
                break;
            case 'checkbox':
                value = true;
                break;
            default: return;
        }
        field.value = value;
    });
    var store = Calculator.Store.create(snapshoot);
    store.setActiveStep('usage');
    return React.createElement(Story, { store: store });
});
stories.add('usage step with lights from example', function () {
    var snapshoot = {
        title: 'Energy LED Payback Calculator',
        subtitle: 'Powering change',
        height: '1000px',
        co2Emissions: 0.527,
        consumption: {
            label: 'Consumption',
            fields: [
                {
                    type: 'input',
                    id: 'averageElectricityPrice',
                    label: 'Average electricity price',
                    placeholder: '10',
                    suffix: 'p/kWh',
                    mandatory: true,
                    value: '10',
                    format: 'number',
                },
                {
                    type: 'input',
                    id: 'occupancyHours',
                    label: 'Occupancy hours per annum',
                    placeholder: '3210',
                    suffix: 'hrs/annum',
                    mandatory: true,
                    format: 'number',
                    value: '3120',
                    helpText: 'Occupancy Hours = Hours per day x Days per week x Weeks per year.' +
                        'So for an office open 6 days a week, 8am - 6pm, 52 weeks a year, the calculation is 6 x 10 x 52 = 3,120.',
                },
                {
                    type: 'checkbox',
                    id: 'lightingControls',
                    label: 'Do you have lighting controls?',
                    helpText: 'Controls typically include motion sensors, daylight harvesting, dimmer switches, ' +
                        'centralised lighting control systems. Click here for more information.',
                    value: true,
                    mandatory: false,
                },
                {
                    type: 'input',
                    id: 'expectedReduction',
                    placeholder: '10',
                    format: 'number',
                    suffix: '%',
                    mandatory: true,
                    label: 'Expected reduction in usage from the controls',
                    showIfChecked: ['lightingControls'],
                    value: '10',
                },
                {
                    type: 'checkbox',
                    id: 'greenBusiness',
                    markdown: true,
                    label: 'Eligible for Green Business Funding',
                    helpText: 'Small and medium businesses, schools, charities can obtain a non-repayable 15% ' +
                        'capital contribution, up to £5,000. Open to Small Medium Enterprises (SME), maintained schools and some' +
                        'others. [Click here to download](www.google.co.uk) more information, ' +
                        'or email [info@spiritenergy.co.uk](mailto:info@spiritenergy.co.uk) or check your availability.',
                    value: true,
                    mandatory: false,
                }
            ]
        },
        usage: {
            label: 'Usage',
            controls: [
                {
                    id: 'none',
                    label: 'None',
                    costPerFitting: 0,
                    reduction: 0,
                },
                {
                    id: 'dimming',
                    label: 'Dimming',
                    costPerFitting: 30,
                    reduction: 0.15,
                },
                {
                    id: 'absenceDetection',
                    label: 'Absense detection',
                    costPerFitting: 50,
                    reduction: 0.3,
                },
                {
                    id: 'presenceDetection',
                    label: 'Presence detection',
                    costPerFitting: 50,
                    reduction: 0.3,
                },
                {
                    id: 'daylightHarvesting',
                    label: 'DaylightHarvesting',
                    costPerFitting: 50,
                    reduction: 0.25,
                },
                {
                    id: 'timedSwitching',
                    label: 'Timed switching',
                    costPerFitting: 20,
                    reduction: 0.15,
                }
            ],
            lights: [],
            availableLights: lights,
        },
        results: {
            label: 'Results'
        },
        activeStep: 'usage'
    };
    var store = Calculator.Store.create(snapshoot);
    store.usage.addLight('flourescentTube72');
    store.usage.lights[0].setCount(30);
    store.usage.addLight('cflBulkhead2d28');
    store.usage.lights[0].setCount(15);
    store.usage.addLight('ceilingPanel144');
    store.usage.lights[0].setCount(50);
    store.usage.lights[0].setControl(store.usage.controls.find(function (c) { return c.id === 'presenceDetection'; }));
    return React.createElement(Story, { store: store });
});
stories.add('results step with lights from example', function () {
    var snapshoot = {
        title: 'Energy LED Payback Calculator',
        subtitle: 'Powering change',
        height: '1000px',
        co2Emissions: 0.527,
        consumption: {
            label: 'Consumption',
            fields: [
                {
                    type: 'input',
                    id: 'averageElectricityPrice',
                    label: 'Average electricity price',
                    placeholder: '10',
                    suffix: 'p/kWh',
                    mandatory: true,
                    value: '10',
                    format: 'number',
                },
                {
                    type: 'input',
                    id: 'occupancyHours',
                    label: 'Occupancy hours per annum',
                    placeholder: '3210',
                    suffix: 'hrs/annum',
                    mandatory: true,
                    format: 'number',
                    value: '3120',
                    helpText: 'Occupancy Hours = Hours per day x Days per week x Weeks per year.' +
                        'So for an office open 6 days a week, 8am - 6pm, 52 weeks a year, the calculation is 6 x 10 x 52 = 3,120.',
                },
                {
                    type: 'checkbox',
                    id: 'lightingControls',
                    label: 'Do you have lighting controls?',
                    helpText: 'Controls typically include motion sensors, daylight harvesting, dimmer switches, ' +
                        'centralised lighting control systems. Click here for more information.',
                    value: true,
                    mandatory: false,
                },
                {
                    type: 'input',
                    id: 'expectedReduction',
                    placeholder: '10',
                    format: 'number',
                    suffix: '%',
                    mandatory: true,
                    label: 'Expected reduction in usage from the controls',
                    showIfChecked: ['lightingControls'],
                    value: '10',
                },
                {
                    type: 'checkbox',
                    id: 'greenBusiness',
                    markdown: true,
                    label: 'Eligible for Green Business Funding',
                    helpText: 'Small and medium businesses, schools, charities can obtain a non-repayable 15% ' +
                        'capital contribution, up to £5,000. Open to Small Medium Enterprises (SME), maintained schools and some' +
                        'others. [Click here to download](www.google.co.uk) more information, ' +
                        'or email [info@spiritenergy.co.uk](mailto:info@spiritenergy.co.uk) or check your availability.',
                    value: true,
                    mandatory: false,
                }
            ]
        },
        usage: {
            label: 'Usage',
            controls: [
                {
                    id: 'none',
                    label: 'None',
                    costPerFitting: 0,
                    reduction: 0,
                },
                {
                    id: 'dimming',
                    label: 'Dimming',
                    costPerFitting: 30,
                    reduction: 0.15,
                },
                {
                    id: 'absenceDetection',
                    label: 'Absence detection',
                    costPerFitting: 50,
                    reduction: 0.3,
                },
                {
                    id: 'presenceDetection',
                    label: 'Presence detection',
                    costPerFitting: 50,
                    reduction: 0.3,
                },
                {
                    id: 'daylightHarvesting',
                    label: 'DaylightHarvesting',
                    costPerFitting: 50,
                    reduction: 0.25,
                },
                {
                    id: 'timedSwitching',
                    label: 'Timed switching',
                    costPerFitting: 20,
                    reduction: 0.15,
                }
            ],
            lights: [],
            availableLights: lights,
        },
        results: {
            label: 'Results'
        },
        activeStep: 'results'
    };
    var store = Calculator.Store.create(snapshoot);
    store.usage.addLight('flourescentTube72');
    store.usage.lights[0].setCount(30);
    store.usage.addLight('cflBulkhead2d28');
    store.usage.lights[0].setCount(15);
    store.usage.addLight('ceilingPanel144');
    store.usage.lights[0].setCount(50);
    store.usage.lights[0].setControl(store.usage.controls.find(function (c) { return c.id === 'presenceDetection'; }));
    return React.createElement(Story, { store: store });
});
var lights = [
    {
        id: 'metalHalide250',
        type: 'classic',
        label: 'Metal Halide',
        size: '',
        power: 250,
        lifetime: 20000,
        maintenanceCost: 5,
        ballasLoss: 0.2,
        maintenanceLabourCost: 8,
        lightReplacement: {
            id: 'ledCornLamp150',
            type: 'led',
            label: 'Corn Lamp',
            size: '',
            power: 150,
            lifetime: 40000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 110,
            installLabourCostPerUnit: 10,
            emergencyPackCostPerUnit: 75,
            emergencyPackInstallLabourCostPerUnit: 120,
        },
    },
    {
        id: 'metalHalide400',
        type: 'classic',
        label: 'Metal Halide',
        size: '',
        power: 400,
        lifetime: 20000,
        maintenanceCost: 3,
        ballasLoss: 0.2,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledHighBay150',
            type: 'led',
            label: 'LED High Bay',
            size: '',
            power: 150,
            lifetime: 50000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 135,
            installLabourCostPerUnit: 35,
            emergencyPackCostPerUnit: 50,
            emergencyPackInstallLabourCostPerUnit: 60,
        }
    },
    {
        id: 'ceilingPanel72',
        type: 'classic',
        label: 'Ceiling Panel',
        size: '600x600mm',
        power: 72,
        lifetime: 20000,
        maintenanceCost: 3,
        ballasLoss: 0.13,
        maintenanceLabourCost: 3,
        lightReplacement: {
            id: 'ledPanel40',
            type: 'led',
            label: 'LED Panel',
            size: '600x600mm',
            power: 40,
            lifetime: 35000,
            maintenanceCost: 2,
            purchaseCostPerUnit: 25,
            installLabourCostPerUnit: 35,
            emergencyPackCostPerUnit: 70,
            emergencyPackInstallLabourCostPerUnit: 60,
        },
    },
    {
        id: 'ceilingPanel108',
        type: 'classic',
        label: 'Ceiling Panel',
        size: '600x1200mm',
        power: 108,
        lifetime: 20000,
        maintenanceCost: 3,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledPanel60',
            type: 'led',
            label: 'LED Panel',
            size: '600x1200mm',
            power: 60,
            lifetime: 30000,
            maintenanceCost: 2,
            purchaseCostPerUnit: 65,
            installLabourCostPerUnit: 35,
            emergencyPackCostPerUnit: 70,
            emergencyPackInstallLabourCostPerUnit: 60,
        },
    },
    {
        id: 'ceilingPanel144',
        type: 'classic',
        label: 'Ceiling Panel',
        size: '600x1200mm',
        power: 144,
        lifetime: 20000,
        maintenanceCost: 6,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledPanel60',
            type: 'led',
            label: 'LED Panel',
            size: '600x1200mm',
            power: 60,
            lifetime: 30000,
            maintenanceCost: 3.5,
            purchaseCostPerUnit: 65,
            installLabourCostPerUnit: 35,
            emergencyPackCostPerUnit: 70,
            emergencyPackInstallLabourCostPerUnit: 60,
        },
    },
    {
        id: 'flourescentTube36',
        type: 'classic',
        label: 'Singe Fluorescent Tube',
        size: '4ft',
        power: 36,
        lifetime: 20000,
        maintenanceCost: 2.5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledSingleTube18',
            type: 'led',
            label: 'LED Tube',
            size: '4ft',
            power: 18,
            lifetime: 30000,
            maintenanceCost: 1.5,
            purchaseCostPerUnit: 5,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 65,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'flourescentTube72',
        type: 'classic',
        label: 'Double Fluorescent Tube',
        size: '4ft',
        power: 72,
        lifetime: 20000,
        maintenanceCost: 5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledDoubleTube36',
            type: 'led',
            label: 'LED Double Tube',
            size: '4ft',
            power: 36,
            lifetime: 30000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 10,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 71,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'flourescentTube58',
        type: 'classic',
        label: 'Single Fluorescent Tube',
        size: '5ft',
        power: 52,
        lifetime: 20000,
        maintenanceCost: 2.5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledSingleTube22',
            type: 'led',
            label: 'LED Tube',
            size: '5ft',
            power: 22,
            lifetime: 30000,
            maintenanceCost: 1.5,
            purchaseCostPerUnit: 6,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 71,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'flourescentTube116',
        type: 'classic',
        label: 'Double Fluorescent Tube',
        size: '5ft',
        power: 116,
        lifetime: 20000,
        maintenanceCost: 5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledDoubleTube44',
            type: 'led',
            label: 'LED Double Tube',
            size: '5ft',
            power: 44,
            lifetime: 30000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 12,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 80,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'flourescentTube70',
        type: 'classic',
        label: 'Single Fluorescent Tube',
        size: '6ft',
        power: 70,
        lifetime: 20000,
        maintenanceCost: 2.5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledSingleTube30',
            type: 'led',
            label: 'LED Tube',
            size: '6ft',
            power: 30,
            lifetime: 40000,
            maintenanceCost: 1.5,
            purchaseCostPerUnit: 14,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 80,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'flourescentTube140',
        type: 'classic',
        label: 'Double Fluorescent Tube',
        size: '6ft',
        power: 140,
        lifetime: 20000,
        maintenanceCost: 5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledDoubleTube60',
            type: 'led',
            label: 'LED Double Tube',
            size: '6ft',
            power: 60,
            lifetime: 40000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 28,
            installLabourCostPerUnit: 17,
            emergencyPackCostPerUnit: 90,
            emergencyPackInstallLabourCostPerUnit: 55,
        },
    },
    {
        id: 'cflBulkhead2d16',
        type: 'classic',
        label: '2D CFL Bulkhead',
        size: '',
        power: 16,
        lifetime: 10000,
        maintenanceCost: 2.5,
        ballasLoss: 0.1,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'led2d9',
            type: 'led',
            label: '2D LED',
            size: '',
            power: 9,
            lifetime: 30000,
            maintenanceCost: 2,
            purchaseCostPerUnit: 11,
            installLabourCostPerUnit: 8,
            emergencyPackCostPerUnit: 35,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'cflBulkhead2d28',
        type: 'classic',
        label: '2D CFL Bulkhead',
        size: '',
        power: 28,
        lifetime: 10000,
        maintenanceCost: 2.5,
        ballasLoss: 0.1,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'led2d12',
            type: 'led',
            label: '2D LED',
            size: '',
            power: 12,
            lifetime: 30000,
            maintenanceCost: 2,
            purchaseCostPerUnit: 17,
            installLabourCostPerUnit: 8,
            emergencyPackCostPerUnit: 30,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'clfBulb30',
        type: 'classic',
        label: 'CFL Bulb',
        size: '',
        power: 30,
        lifetime: 10000,
        maintenanceCost: 1.5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledBulb5',
            type: 'led',
            label: 'LED Bulb',
            size: '',
            power: 5,
            lifetime: 25000,
            maintenanceCost: 1,
            purchaseCostPerUnit: 2.5,
            installLabourCostPerUnit: 4,
            emergencyPackCostPerUnit: 50,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'incandescentBulb60',
        type: 'classic',
        label: 'Incandescent Bulb',
        size: '',
        power: 60,
        lifetime: 1000,
        maintenanceCost: 2.5,
        ballasLoss: 0,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledBulb6',
            type: 'led',
            label: 'LED Bulb',
            size: '',
            power: 6,
            lifetime: 25000,
            maintenanceCost: 1,
            purchaseCostPerUnit: 2.75,
            installLabourCostPerUnit: 4,
            emergencyPackCostPerUnit: 50,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'halogenBulb',
        type: 'classic',
        label: 'Halogen Bulb',
        size: '',
        power: 30,
        lifetime: 2000,
        maintenanceCost: 1.5,
        ballasLoss: 0,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledBulb4',
            type: 'led',
            label: 'LED Bulb',
            size: '',
            power: 4,
            lifetime: 25000,
            maintenanceCost: 1,
            purchaseCostPerUnit: 2.5,
            installLabourCostPerUnit: 4,
            emergencyPackCostPerUnit: 50,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'halogenSpotlight50',
        type: 'classic',
        label: 'Halogen Spotlight - GU10',
        size: '',
        power: 50,
        lifetime: 2000,
        maintenanceCost: 1.5,
        ballasLoss: 0,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledSpotlight5',
            type: 'led',
            label: 'LED Spotlight',
            size: '',
            power: 5,
            lifetime: 25000,
            maintenanceCost: 1,
            purchaseCostPerUnit: 2.5,
            installLabourCostPerUnit: 5,
            emergencyPackCostPerUnit: 50,
            emergencyPackInstallLabourCostPerUnit: 40,
        },
    },
    {
        id: 'halogenFoodlight180',
        type: 'classic',
        label: 'Halogen Foodlight',
        size: '',
        power: 180,
        lifetime: 2000,
        maintenanceCost: 5,
        ballasLoss: 0,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledFloodlight50',
            type: 'led',
            label: 'LED Floodlight',
            size: '',
            power: 50,
            lifetime: 30000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 35,
            installLabourCostPerUnit: 40,
            emergencyPackCostPerUnit: 75,
            emergencyPackInstallLabourCostPerUnit: 60,
        }
    },
    {
        id: 'pressureSodiumLow',
        type: 'classic',
        label: 'Low Pressure Sodium',
        size: '',
        power: 90,
        lifetime: 16000,
        maintenanceCost: 4,
        ballasLoss: 0.13,
        maintenanceLabourCost: 2.5,
        lightReplacement: {
            id: 'ledCornLamp75',
            type: 'led',
            label: 'Corn Lamp',
            size: '',
            power: 75,
            lifetime: 40000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 60,
            installLabourCostPerUnit: 8,
            emergencyPackCostPerUnit: 75,
            emergencyPackInstallLabourCostPerUnit: 120,
        },
    },
    {
        id: 'pressureSodiumHigh',
        type: 'classic',
        label: 'High Pressure Sodium',
        size: '',
        power: 150,
        lifetime: 14000,
        maintenanceCost: 5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 8,
        lightReplacement: {
            id: 'ledCornLamp100',
            type: 'led',
            label: 'Corn Lamp',
            size: '',
            power: 100,
            lifetime: 40000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 90,
            installLabourCostPerUnit: 8,
            emergencyPackCostPerUnit: 75,
            emergencyPackInstallLabourCostPerUnit: 120,
        },
    },
    {
        id: 'mercuryVapourLamp',
        type: 'classic',
        label: 'Mercury Vapour Lamp',
        size: '',
        power: 150,
        lifetime: 18000,
        maintenanceCost: 5,
        ballasLoss: 0.13,
        maintenanceLabourCost: 8,
        lightReplacement: {
            id: 'ledCornLamp100',
            type: 'led',
            label: 'Corn Lamp',
            size: '',
            power: 100,
            lifetime: 40000,
            maintenanceCost: 3,
            purchaseCostPerUnit: 90,
            installLabourCostPerUnit: 8,
            emergencyPackCostPerUnit: 75,
            emergencyPackInstallLabourCostPerUnit: 120,
        },
    },
];
//# sourceMappingURL=Calculator.story.js.map