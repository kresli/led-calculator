var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import classNames from 'classnames';
var Dropdown = (function (_super) {
    __extends(Dropdown, _super);
    function Dropdown() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.visibleMenu = false;
        return _this;
    }
    Object.defineProperty(Dropdown.prototype, "rootClassName", {
        get: function () {
            return classNames({
                'dropdown': true,
                'is-active': this.visibleMenu,
            });
        },
        enumerable: true,
        configurable: true
    });
    Dropdown.prototype.toggleVisibleMenu = function () {
        this.visibleMenu = !this.visibleMenu;
    };
    Dropdown.prototype.hideVisibleMenu = function () {
        this.visibleMenu = false;
    };
    Dropdown.prototype.onChange = function (id) {
        this.hideVisibleMenu();
        this.props.onChange(id);
    };
    Object.defineProperty(Dropdown.prototype, "menu", {
        get: function () {
            var _this = this;
            var _a = this.props, items = _a.items, maxHeight = _a.maxHeight, separator = _a.separator, fullWidth = _a.fullWidth;
            var maxHeightCalculated = maxHeight ? maxHeight + "px" : 'auto';
            var itemsElements = [];
            var menuStyle = {
                width: fullWidth ? '100%' : 'auto',
            };
            items.forEach(function (item, i) {
                itemsElements.push((React.createElement("a", { key: item.id, className: "dropdown-item", onClick: function () { return _this.onChange(item.id); } }, item.label)));
                if (separator) {
                    itemsElements.push((React.createElement("hr", { key: i, className: "dropdown-divider" })));
                }
            });
            return (React.createElement("div", { className: "dropdown-menu", id: "dropdown-menu", role: "menu", style: menuStyle },
                React.createElement("div", { className: "dropdown-content", style: { maxHeight: maxHeightCalculated, overflow: 'auto' } }, itemsElements)));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Dropdown.prototype, "selectedLabel", {
        get: function () {
            var _a = this.props, selected = _a.selected, items = _a.items;
            var selectedItem = items.find(function (i) { return i.id === selected; });
            return !selectedItem ? '' : selectedItem.label;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Dropdown.prototype, "label", {
        get: function () {
            var _a = this.props, type = _a.type, label = _a.label;
            switch (type) {
                case 'select': return React.createElement("span", { style: { flexWrap: 'wrap', justifyContent: 'center', display: 'flex' } },
                    React.createElement("strong", null, this.selectedLabel),
                    React.createElement("span", { style: { marginLeft: '1em' } }, label));
                case 'picker': return React.createElement("span", null, label);
                default: return React.createElement("div", null);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Dropdown.prototype, "rootStyle", {
        get: function () {
            var fullWidth = this.props.fullWidth;
            return {
                flex: 1,
                width: fullWidth ? '100%' : 'auto',
            };
        },
        enumerable: true,
        configurable: true
    });
    Dropdown.prototype.render = function () {
        return (React.createElement("div", { className: this.rootClassName, onMouseLeave: this.hideVisibleMenu, style: this.rootStyle },
            React.createElement("div", { className: "dropdown-trigger", style: { flex: 1, display: 'flex' } },
                React.createElement("button", { style: { display: 'flex', height: 'auto', flex: 1 }, className: "button", "aria-haspopup": "true", "aria-controls": "dropdown-menu", onClick: this.toggleVisibleMenu },
                    this.label,
                    React.createElement("span", { className: "icon is-small" },
                        React.createElement("i", { className: "fa fa-angle-down", "aria-hidden": "true" })))),
            this.menu));
    };
    Dropdown.defaultProps = {
        type: 'select',
        separator: false,
        fullWidth: false,
    };
    __decorate([
        observable
    ], Dropdown.prototype, "visibleMenu", void 0);
    __decorate([
        computed
    ], Dropdown.prototype, "rootClassName", null);
    __decorate([
        action.bound
    ], Dropdown.prototype, "toggleVisibleMenu", null);
    __decorate([
        action.bound
    ], Dropdown.prototype, "hideVisibleMenu", null);
    __decorate([
        action.bound
    ], Dropdown.prototype, "onChange", null);
    __decorate([
        computed
    ], Dropdown.prototype, "menu", null);
    __decorate([
        computed
    ], Dropdown.prototype, "selectedLabel", null);
    __decorate([
        computed
    ], Dropdown.prototype, "label", null);
    __decorate([
        computed
    ], Dropdown.prototype, "rootStyle", null);
    Dropdown = __decorate([
        observer
    ], Dropdown);
    return Dropdown;
}(React.Component));
export default Dropdown;
//# sourceMappingURL=Dropdown.js.map