// import { types } from 'mobx-state-tree';
//
// const StepStore = types
//   .model({
//     label: types.string,
//     id: types.string,
//     active: types.boolean,
//     valid: types.boolean,
//   })
//   .actions(self => ({
//     onInput: () => {
//
//     }
//   }));
//
// export const Store = types
//   .model({
//     steps: types.array(StepStore)
//   })
//   .views(self => ({
//     get stepper() {
//       return {
//         steps: self.steps.map( ({id, label}) => ({id, label}) ),
//         defaultStepId: self.steps[0].id,
//       };
//     },
//     get activeStep() {
//       return self.steps.find( s => s.active );
//     },
//     get activeStepIndex() {
//       return self.steps.indexOf( this.activeStep );
//     }
//   }))
//   .actions( self => {
//     function setActiveStep(stepId: string){
//       self.steps.forEach( s => s.active = false );
//       (self.steps.find( s => s.id === stepId ) || self.steps[0]).active = true;
//     }
//     // function setActiveStepNext() {
//     //   if ((self.activeStepIndex !== undefined) && this.activeStep && this.activeStep.valid) {
//     //     const nextIndex = this.activeStepIndex !== this.steps.length - 1 ? this.activeStepIndex + 1 : this.activeStepIndex;
//     //     this.setActiveStep(this.steps[nextIndex]);
//     //   };
//     // }
//     // function setActiveStepPrevious() {
//     //   if (this.activeStepIndex === undefined) return;
//     //   const nextIndex = this.activeStepIndex !== 0 ? this.activeStepIndex - 1 : this.activeStepIndex;
//     //   this.setActiveStep(this.steps[nextIndex]);
//     // }
//     return {setActiveStep};
//   });
//# sourceMappingURL=Store.js.map