var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { observer } from 'mobx-react';
import * as Form from '../../../Form';
import { types, detach } from 'mobx-state-tree';
// import * as Stepper from '../../Stepper';
import { CardContent } from 'bloomer';
import { action } from 'mobx';
import * as Field from '../../../Field';
var forms = new Map();
export var Store = types
    .model('FormConsuptionStore', {
    label: types.string,
    fields: types.array(Field.Store),
})
    .actions(function (self) {
    function afterCreate() {
        var form = Form.Store.create({
            fields: self.fields.map(function (s) { return detach(s); }),
        });
        forms.set(self, form);
    }
    return { afterCreate: afterCreate };
})
    .views(function (self) { return ({
    get form() {
        var form = forms.get(self);
        return form;
    },
    get valid() {
        var form = forms.get(self);
        return form.valid;
    },
}); })
    .views(function (self) { return ({
    get data() {
        return self.form.fields.map(function (_a) {
            var id = _a.id, label = _a.label, value = _a.value;
            return ({ id: id, label: label, value: value });
        });
    }
}); });
var Consumption = (function (_super) {
    __extends(Consumption, _super);
    function Consumption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Consumption.prototype.componentWillMount = function () {
        this.checkValid();
    };
    Consumption.prototype.onInput = function () {
        this.checkValid();
    };
    Consumption.prototype.checkValid = function () {
        // const {stepper, form} = this.props.store;
        // if (form.valid) {
        //   stepper.disableAfterActiveStep(false);
        // } else {
        //   stepper.disableAfterActiveStep(true);
        // }
    };
    Consumption.prototype.render = function () {
        return (React.createElement(CardContent, null,
            React.createElement(Form.Form, { store: this.props.store.form, onInput: this.onInput })));
    };
    __decorate([
        action.bound
    ], Consumption.prototype, "onInput", null);
    Consumption = __decorate([
        observer
    ], Consumption);
    return Consumption;
}(React.Component));
export { Consumption };
//# sourceMappingURL=Consumption.js.map