var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { types } from 'mobx-state-tree';
import { CardContent, Table } from 'bloomer';
import { computed } from 'mobx';
export var Store = types
    .model('ResultsStore')
    .props({
    label: types.string,
});
var style = {
    table: {
        title: {
            textAlign: 'center',
            fontWeight: 'bold',
            background: '#02d0b2',
            color: 'white',
        }
    }
};
var Cell = function (_a) {
    var value = _a.value, prefix = _a.prefix, suffix = _a.suffix;
    return (React.createElement("td", null,
        prefix && React.createElement("small", null, prefix),
        React.createElement("strong", null, value),
        suffix && React.createElement("small", null, suffix)));
};
var SummaryOfSaving = (function (_super) {
    __extends(SummaryOfSaving, _super);
    function SummaryOfSaving() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(SummaryOfSaving.prototype, "greenBusiness", {
        get: function () {
            var prettySummary = this.props.calculatorStore.prettySummary;
            var existing = prettySummary.newLightsWithExistingControls;
            var proposed = prettySummary.newLightsWithProposedAdditionalControls;
            if (!proposed.greenBusiness)
                return null;
            var title = (React.createElement("tr", null,
                React.createElement("td", { colSpan: 4, style: style.table.title }, "Using Carbon Trust Green Business Fund")));
            var contribution = (React.createElement("tr", { title: "pro-rated if payback > 5 years, maximum contribution £5,000, minimum project size £5,000" },
                React.createElement("td", null, "15% contribution"),
                React.createElement("td", null),
                React.createElement(Cell, { value: existing.greenBusinessContribution, prefix: "£" }),
                React.createElement(Cell, { value: proposed.greenBusinessContribution, prefix: "£" })));
            var upgrade = (React.createElement("tr", null,
                React.createElement("td", null, "Reduced upgrade cost"),
                React.createElement("td", null),
                React.createElement(Cell, { value: existing.greenBusinessReducedUpgradeCost, prefix: "~£", suffix: " (excl. VAT)" }),
                React.createElement(Cell, { value: proposed.greenBusinessReducedUpgradeCost, prefix: "~£", suffix: " (excl. VAT)" })));
            var payback = (React.createElement("tr", null,
                React.createElement("td", null, "Payback time"),
                React.createElement("td", null),
                React.createElement(Cell, { value: existing.greenBusinessPaybackTime, suffix: " years" }),
                React.createElement(Cell, { value: proposed.greenBusinessPaybackTime, suffix: " years" })));
            return [title, contribution, upgrade, payback];
        },
        enumerable: true,
        configurable: true
    });
    SummaryOfSaving.prototype.render = function () {
        var prettySummary = this.props.calculatorStore.prettySummary;
        var current = prettySummary.currentLightsWithExistingControls;
        var existing = prettySummary.newLightsWithExistingControls;
        var proposed = prettySummary.newLightsWithProposedAdditionalControls;
        return (React.createElement(Table, { isStriped: true, isNarrow: true, className: "is-hoverable is-fullwidth" },
            React.createElement("thead", null,
                React.createElement("tr", null,
                    React.createElement("th", null),
                    React.createElement("th", null, "Current lights + existing controls"),
                    React.createElement("th", null, "New lights + existing controls"),
                    React.createElement("th", null, "New lights + proposed additional controls"))),
            React.createElement("tbody", null,
                React.createElement("tr", null,
                    React.createElement("td", { colSpan: 4, style: style.table.title }, "Load data and running hours")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Max load"),
                    React.createElement(Cell, { value: current.maxLoad, suffix: "kW" }),
                    React.createElement(Cell, { value: existing.maxLoad, suffix: "kW" }),
                    React.createElement(Cell, { value: proposed.maxLoad, suffix: "kW" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Occupancy"),
                    React.createElement(Cell, { value: current.hoursPerAnnum, suffix: "hrs/year" }),
                    React.createElement(Cell, { value: existing.hoursPerAnnum, suffix: "hrs/year" }),
                    React.createElement(Cell, { value: proposed.hoursPerAnnum, suffix: "hrs/year" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Running"),
                    React.createElement(Cell, { value: current.runningHours, prefix: "~", suffix: "hrs/year" }),
                    React.createElement(Cell, { value: existing.runningHours, prefix: "~", suffix: "hrs/year" }),
                    React.createElement(Cell, { value: proposed.runningHours, prefix: "~", suffix: "hrs/year" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Consumption"),
                    React.createElement(Cell, { value: current.consumption, suffix: "kWh/year" }),
                    React.createElement(Cell, { value: existing.consumption, suffix: "kWh/year" }),
                    React.createElement(Cell, { value: proposed.consumption, suffix: "kWh/year" })),
                React.createElement("tr", null,
                    React.createElement("td", { colSpan: 4, style: style.table.title }, "Savings")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Reduction"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.reduction, suffix: "kWh/year" }),
                    React.createElement(Cell, { value: proposed.reduction, suffix: "kWh/year" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Of current Usage"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.percentageOfUsage, suffix: "%" }),
                    React.createElement(Cell, { value: proposed.percentageOfUsage, suffix: "%" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Electricity reduction"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.electricityCostReduction, prefix: "£", suffix: "/year" }),
                    React.createElement(Cell, { value: proposed.electricityCostReduction, prefix: "£", suffix: "/year" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Maintenance reduction"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.maintenanceCostReduction, prefix: "£", suffix: "/year" }),
                    React.createElement(Cell, { value: proposed.maintenanceCostReduction, prefix: "£", suffix: "/year" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Total savings"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.totalSaving, prefix: "£", suffix: "/year" }),
                    React.createElement(Cell, { value: proposed.totalSaving, prefix: "£", suffix: "/year" })),
                React.createElement("tr", null,
                    React.createElement("td", { colSpan: 4, style: style.table.title }, "Cost and Payback time")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Upgrade cost"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.costOfUpgrade, prefix: "~£", suffix: " (excl. VAT)" }),
                    React.createElement(Cell, { value: proposed.costOfUpgrade, prefix: "~£", suffix: " (excl. VAT)" })),
                React.createElement("tr", null,
                    React.createElement("td", null, "Payback time"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.paybackTime, suffix: " years" }),
                    React.createElement(Cell, { value: proposed.paybackTime, suffix: " years" })),
                this.greenBusiness,
                React.createElement("tr", null,
                    React.createElement("td", { colSpan: 4, style: style.table.title }, "Reduction in carbon footprint")),
                React.createElement("tr", null,
                    React.createElement("td", null, "CO\u2082 reduction"),
                    React.createElement("td", null),
                    React.createElement(Cell, { value: existing.co2Reduction, suffix: "tonnes/year" }),
                    React.createElement(Cell, { value: proposed.co2Reduction, suffix: "tonnes/year" })))));
    };
    __decorate([
        computed
    ], SummaryOfSaving.prototype, "greenBusiness", null);
    return SummaryOfSaving;
}(React.Component));
var Results = (function (_super) {
    __extends(Results, _super);
    function Results() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Results.prototype.render = function () {
        return (React.createElement("div", { style: { position: 'absolute', top: '15.1em', width: '100%', bottom: '3.1em', overflowY: 'auto' } },
            React.createElement(CardContent, null,
                React.createElement(SummaryOfSaving, __assign({}, this.props)))));
    };
    return Results;
}(React.Component));
export { Results };
//# sourceMappingURL=Results.js.map