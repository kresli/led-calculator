import { types } from 'mobx-state-tree';
export var Store = types.model('ControlStore')
    .props({
    id: types.identifier(),
    label: types.string,
    costPerFitting: types.number,
    reduction: types.number,
});
//# sourceMappingURL=Control.js.map