var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import * as Control from './Control';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import Dropdown from '../../Dropdown';
import { types, getSnapshot, getType } from 'mobx-state-tree';
import uniqid from 'uniqid';
import { Button } from 'bloomer';
var sharedLightProps = {
    id: types.identifier(),
    label: types.string,
    image: types.optional(types.string, ''),
    size: types.string,
    power: types.number,
    lifetime: types.number,
    maintenanceCost: types.number,
};
export var LedLightStore = types.model('LedLight')
    .props(__assign({}, sharedLightProps, { type: types.literal('led'), purchaseCostPerUnit: types.number, installLabourCostPerUnit: types.number, emergencyPackCostPerUnit: types.number, emergencyPackInstallLabourCostPerUnit: types.number }));
export var ClassicLightStore = types.model('ClassicLight')
    .props(__assign({}, sharedLightProps, { type: types.literal('classic'), count: types.optional(types.number, 1), maintenanceLabourCost: types.number, ballasLoss: types.number, lightReplacement: LedLightStore, control: types.optional(types.reference(Control.Store), 'none'), controlInUse: types.optional(types.boolean, false), occupancyHoursPerAnnum: types.optional(types.number, 1), electricityPrice: types.optional(types.number, 1), emergencyLightCount: types.optional(types.number, 0), co2Emissions: types.optional(types.number, 0) }))
    .views(function (self) { return ({
    get active() {
        return self.count > 0;
    },
}); })
    .views(function (self) { return ({
    get effectiveLoad() {
        var load = (self.power / 1000) * (1 + self.ballasLoss) * self.count;
        return load;
    }
}); })
    .views(function (self) { return ({
    get annualLoadWithExistingControls() {
        return self.effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
    }
}); })
    .views(function (self) { return ({
    get annualUsage() {
        return self.effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
    }
}); })
    .views(function (self) { return ({
    get electricityCostPerYear() {
        return self.annualUsage * (self.electricityPrice / 100);
    }
}); })
    .views(function (self) { return ({
    get annualMaintenanceCost() {
        var maintenanceCost = self.maintenanceCost, maintenanceLabourCost = self.maintenanceLabourCost, occupancyHoursPerAnnum = self.occupancyHoursPerAnnum, control = self.control, lifetime = self.lifetime, count = self.count;
        return (maintenanceCost + maintenanceLabourCost) * ((occupancyHoursPerAnnum * (1 - control.reduction)) / lifetime) * count;
    }
}); })
    .views(function (self) { return ({
    get totalRunningCost() {
        var electricityCostPerYear = self.electricityCostPerYear, annualMaintenanceCost = self.annualMaintenanceCost;
        return electricityCostPerYear + annualMaintenanceCost;
    }
}); })
    .views(function (self) { return ({
    get ledData() {
        var _a = self.lightReplacement, label = _a.label, power = _a.power, size = _a.size, purchaseCostPerUnit = _a.purchaseCostPerUnit, installLabourCostPerUnit = _a.installLabourCostPerUnit, emergencyPackCostPerUnit = _a.emergencyPackCostPerUnit, emergencyPackInstallLabourCostPerUnit = _a.emergencyPackInstallLabourCostPerUnit, maintenanceCost = _a.maintenanceCost, lifetime = _a.lifetime;
        var effectiveLoad = (power / 1000) * self.count;
        var annualLoadWithExistingControls = effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
        var annualElectricityCost = annualLoadWithExistingControls * (self.electricityPrice / 100);
        var installCostPerUnit = purchaseCostPerUnit + installLabourCostPerUnit;
        var upgradeCost = installCostPerUnit * self.count + (emergencyPackCostPerUnit + emergencyPackInstallLabourCostPerUnit) * self.emergencyLightCount;
        var annumnMaintenanceCost = (maintenanceCost + self.maintenanceLabourCost) * ((self.occupancyHoursPerAnnum * (1 - self.control.reduction)) / lifetime) * self.count;
        var co2Reduction = (self.annualUsage - annualLoadWithExistingControls) * self.co2Emissions / 1000;
        return {
            label: label,
            power: power,
            size: size,
            effectiveLoad: effectiveLoad,
            annualLoadWithExistingControls: annualLoadWithExistingControls,
            annualElectricityCost: annualElectricityCost,
            purchaseCostPerUnit: purchaseCostPerUnit,
            installLabourCostPerUnit: installLabourCostPerUnit,
            installCostPerUnit: installCostPerUnit,
            emergencyPackCostPerUnit: emergencyPackCostPerUnit,
            emergencyPackInstallLabourCostPerUnit: emergencyPackInstallLabourCostPerUnit,
            upgradeCost: upgradeCost,
            maintenanceCost: maintenanceCost,
            maintenanceLabourCost: self.maintenanceLabourCost,
            lifetime: lifetime,
            annualMaintenanceCost: annumnMaintenanceCost,
            co2Reduction: co2Reduction,
            co2Emissions: self.co2Emissions,
        };
    }
}); })
    .views(function (self) { return ({
    get classicData() {
        var label = self.label, size = self.size, power = self.power, ballasLoss = self.ballasLoss, control = self.control, effectiveLoad = self.effectiveLoad, annualUsage = self.annualUsage, electricityCostPerYear = self.electricityCostPerYear, maintenanceCost = self.maintenanceCost, lifetime = self.lifetime, annualMaintenanceCost = self.annualMaintenanceCost, electricityPrice = self.electricityPrice, occupancyHoursPerAnnum = self.occupancyHoursPerAnnum, count = self.count, maintenanceLabourCost = self.maintenanceLabourCost, totalRunningCost = self.totalRunningCost;
        return {
            count: count,
            size: size,
            electricityPrice: electricityPrice,
            occupancyHoursPerAnnum: occupancyHoursPerAnnum,
            label: label,
            power: power,
            ballasLoss: ballasLoss,
            controls: control.label,
            controlsSaving: control.reduction,
            effectiveLoad: effectiveLoad,
            annualUsage: annualUsage,
            electricityCostPerYear: electricityCostPerYear,
            maintenanceCostPerLight: maintenanceCost,
            miantenanceLabourCostPerLight: maintenanceLabourCost,
            lifetime: lifetime,
            annualMaintenanceCost: annualMaintenanceCost,
            totalRunningCost: totalRunningCost,
        };
    }
}); })
    .views(function (self) { return ({
    get controlData() {
        var ledData = self.ledData;
        var _a = self.control, label = _a.label, reduction = _a.reduction, costPerFitting = _a.costPerFitting;
        var increaseCostOfUpgrade = costPerFitting * self.count;
        var revisedMaintenanceCost = ledData.annualMaintenanceCost * (1 - reduction);
        var annualLoadWithAdditionalControls = ledData.annualLoadWithExistingControls * (1 - reduction);
        var reducedAnnualElectricityCost = annualLoadWithAdditionalControls * (self.electricityPrice / 100);
        var totalCo2Reduction = (self.annualUsage - annualLoadWithAdditionalControls) * self.co2Emissions / 1000;
        return {
            label: label,
            reduction: reduction,
            increaseCostOfUpgrade: increaseCostOfUpgrade,
            revisedMaintenanceCost: revisedMaintenanceCost,
            annualLoadWithAdditionalControls: annualLoadWithAdditionalControls,
            reducedAnnualElectricityCost: reducedAnnualElectricityCost,
            totalCo2Reduction: totalCo2Reduction
        };
    }
}); })
    .views(function (self) { return ({
    get data() {
        return {
            classic: self.classicData,
            led: self.ledData,
            control: self.controlData,
        };
    }
}); })
    .actions(function (self) {
    function setOccupancyHoursPerAnnun(hours) {
        self.occupancyHoursPerAnnum = hours;
    }
    function setElectricityPrice(price) {
        self.electricityPrice = price;
    }
    function setCount(count) {
        self.count = count;
    }
    function clone() {
        var snapshoot = JSON.parse(JSON.stringify(getSnapshot(self)));
        snapshoot.id = snapshoot.id + "_" + uniqid;
        return getType(self).create(snapshoot);
    }
    function setControl(control) {
        self.control = control;
    }
    function setControlInUse(inUse) {
        self.controlInUse = inUse;
    }
    function setCo2Emissions(emissions) {
        self.co2Emissions = emissions;
    }
    return { setCount: setCount, clone: clone, setControl: setControl, setControlInUse: setControlInUse, setOccupancyHoursPerAnnun: setOccupancyHoursPerAnnun, setElectricityPrice: setElectricityPrice, setCo2Emissions: setCo2Emissions };
});
var Light = (function (_super) {
    __extends(Light, _super);
    function Light() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.showBreakdown = true;
        _this.breakdownVisible = false;
        return _this;
    }
    Light.prototype.onSetCountInput = function (ev) {
        this.props.store.setCount(parseInt(ev.currentTarget.value, 10));
    };
    Light.prototype.delete = function () {
        this.props.onDelete(this.props.store);
    };
    Object.defineProperty(Light.prototype, "breakdown", {
        get: function () {
            var _this = this;
            if (!this.showBreakdown)
                return null;
            var breakdown = !this.breakdownVisible ? null : (React.createElement("pre", null, JSON.stringify(this.props.store.data, null, '  ')));
            return (React.createElement("div", { style: { display: 'flex', flexDirection: 'column' } },
                React.createElement(Button, { onClick: function () { return _this.breakdownVisible = !_this.breakdownVisible; }, className: "is-text", style: { flex: 1 } }, "Breakdown"),
                breakdown));
        },
        enumerable: true,
        configurable: true
    });
    Light.prototype.render = function () {
        var _a = this.props, controls = _a.controls, store = _a.store;
        var label = store.label, size = store.size, power = store.power, count = store.count;
        return (React.createElement("article", { className: "box media", style: { display: 'flex' } },
            React.createElement("figure", { className: "media-left is-hidden-mobile" },
                React.createElement("p", { className: "image is-64x64" },
                    React.createElement("img", { src: "https://bulma.io/images/placeholders/128x128.png" }))),
            React.createElement("div", { className: "media-content" },
                React.createElement("div", { className: "content", style: { display: 'flex', flexWrap: 'wrap' } },
                    React.createElement("span", { style: { display: 'inline-flex', justifyContent: 'space-between', flexWrap: 'wrap', marginRight: '1em', marginBottom: '1em' } },
                        React.createElement("span", { style: { display: 'flex', flexDirection: 'row' } },
                            React.createElement("div", { className: "field has-addons", style: { display: 'inline-flex' } },
                                React.createElement("span", { className: "control" },
                                    React.createElement("a", { className: "button is-static" },
                                        React.createElement("strong", null, label),
                                        " ",
                                        React.createElement("small", { style: { marginLeft: '0.5em' } }, size),
                                        " ",
                                        React.createElement("small", { style: { marginLeft: '0.5em' } },
                                            power,
                                            "W"))),
                                React.createElement("div", { className: "control", style: { width: '4em' } },
                                    React.createElement("input", { className: "input", type: "number", placeholder: "24", min: "1", onInput: this.onSetCountInput, onChange: this.onSetCountInput, value: count }))))),
                    React.createElement(ControlSwitch, { controls: controls, lightStore: store })),
                this.breakdown),
            React.createElement("div", { className: "media-right" },
                React.createElement("button", { className: "delete", onClick: this.delete }))));
    };
    __decorate([
        observable
    ], Light.prototype, "showBreakdown", void 0);
    __decorate([
        observable
    ], Light.prototype, "breakdownVisible", void 0);
    __decorate([
        action.bound
    ], Light.prototype, "onSetCountInput", null);
    __decorate([
        action.bound
    ], Light.prototype, "delete", null);
    __decorate([
        computed
    ], Light.prototype, "breakdown", null);
    Light = __decorate([
        observer
    ], Light);
    return Light;
}(React.Component));
export { Light };
var ControlSwitch = (function (_super) {
    __extends(ControlSwitch, _super);
    function ControlSwitch() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ControlSwitch.prototype.onChangeControl = function (id) {
        var controls = this.props.controls;
        var current = controls.find(function (c) { return c.id === id; });
        if (!current)
            return;
        this.props.lightStore.setControl(current);
    };
    ControlSwitch.prototype.onChangeInUse = function () {
        var inUse = !this.props.lightStore.controlInUse;
        this.props.lightStore.setControlInUse(inUse);
    };
    Object.defineProperty(ControlSwitch.prototype, "dropdownItems", {
        get: function () {
            return this.props.controls.map(function (c) { return ({
                id: c.id.toString(),
                label: c.label,
            }); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ControlSwitch.prototype, "checkboxAlreadyInUse", {
        get: function () {
            var _a = this.props.lightStore, controlInUse = _a.controlInUse, control = _a.control;
            if (control.id.toString() === 'none')
                return null;
            var title = 'If the control is already in use please tick the box, otherwise the price of the new control will be added to the final cost';
            return (React.createElement("div", { className: "field", onClick: this.onChangeInUse, style: { marginLeft: '1em' } },
                React.createElement("input", { type: "checkbox", name: "test", className: "switch", checked: controlInUse, onChange: function () { } }),
                React.createElement("label", { title: title }, "control already bought")));
        },
        enumerable: true,
        configurable: true
    });
    ControlSwitch.prototype.render = function () {
        var control = this.props.lightStore.control;
        return (React.createElement("div", { style: { display: 'flex', alignItems: 'center', marginBottom: '1em' } },
            React.createElement(Dropdown, { items: this.dropdownItems, selected: control.id.toString(), label: "Controls", onChange: this.onChangeControl }),
            this.checkboxAlreadyInUse));
    };
    __decorate([
        action.bound
    ], ControlSwitch.prototype, "onChangeControl", null);
    __decorate([
        action.bound
    ], ControlSwitch.prototype, "onChangeInUse", null);
    __decorate([
        computed
    ], ControlSwitch.prototype, "dropdownItems", null);
    __decorate([
        computed
    ], ControlSwitch.prototype, "checkboxAlreadyInUse", null);
    ControlSwitch = __decorate([
        observer
    ], ControlSwitch);
    return ControlSwitch;
}(React.Component));
//# sourceMappingURL=Light.js.map