var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { observer } from 'mobx-react';
import { action, computed } from 'mobx';
import * as Light from './Light';
import * as Control from './Control';
import { CardContent } from 'bloomer';
import Dropdown from '../../Dropdown';
import { types } from 'mobx-state-tree';
export var Store = types.model('Usage')
    .props({
    label: types.string,
    controls: types.array(Control.Store),
    lights: types.optional(types.array(Light.ClassicLightStore), []),
    availableLights: types.array(Light.ClassicLightStore),
    occupancyHours: types.optional(types.number, 0),
    electricityPrice: types.optional(types.number, 0),
    co2Emissions: types.optional(types.number, 0),
    greenBusiness: types.optional(types.boolean, false),
})
    .views(function (self) { return ({
    get currentLightsWithExistingControls() {
        var maxLoad = self.lights.reduce(function (load, l) { return load + l.data.classic.effectiveLoad; }, 0);
        var hoursPerAnnum = self.occupancyHours;
        var consumption = self.lights.reduce(function (load, l) { return load + l.data.classic.annualUsage; }, 0);
        var electricityCost = self.lights.reduce(function (load, l) { return load + l.data.classic.electricityCostPerYear; }, 0);
        var maintenanceCost = self.lights.reduce(function (load, l) { return load + l.data.classic.annualMaintenanceCost; }, 0);
        var totalRunningCost = self.lights.reduce(function (load, l) { return load + l.data.classic.totalRunningCost; }, 0);
        var runningHours = consumption / maxLoad;
        return {
            maxLoad: maxLoad,
            hoursPerAnnum: hoursPerAnnum,
            runningHours: runningHours,
            consumption: consumption,
            electricityCost: electricityCost,
            maintenanceCost: maintenanceCost,
            totalRunningCost: totalRunningCost,
        };
    }
}); })
    .views(function (self) { return ({
    get newLightsWithExistingControls() {
        var current = self.currentLightsWithExistingControls;
        var maxLoad = self.lights.reduce(function (load, l) { return load + l.data.led.effectiveLoad; }, 0);
        var hoursPerAnnum = current.hoursPerAnnum;
        var consumption = self.lights.reduce(function (load, l) { return load + l.data.led.annualLoadWithExistingControls; }, 0);
        var runningHours = consumption / maxLoad;
        var electricityCost = self.lights.reduce(function (load, l) { return load + l.data.led.annualElectricityCost; }, 0);
        var reduction = current.consumption - consumption;
        var maintenanceCost = self.lights.reduce(function (load, l) { return load + l.data.led.annualMaintenanceCost; }, 0);
        var percentageOfUsage = reduction / current.consumption;
        var electricityCostReduction = current.electricityCost - electricityCost;
        var maintenanceCostReduction = current.maintenanceCost - maintenanceCost;
        var totalSaving = electricityCostReduction + maintenanceCostReduction;
        var costOfUpgrade = self.lights.reduce(function (load, l) { return load + l.data.led.upgradeCost; }, 0);
        var paybackTime = costOfUpgrade / totalSaving;
        var greenBusinessContribution = (self.greenBusiness && costOfUpgrade > 5000) ? Math.min(5000, costOfUpgrade * 0.15 * Math.min(1, 5 / (costOfUpgrade / totalSaving))) : 0;
        var greenBusinessReducedUpgradeCost = self.greenBusiness ? costOfUpgrade - greenBusinessContribution : 0;
        var greenBusinessPaybackTime = self.greenBusiness ? greenBusinessReducedUpgradeCost / totalSaving : 0;
        var co2Reduction = self.lights.reduce(function (load, l) { return load + l.data.led.co2Reduction; }, 0);
        return {
            maxLoad: maxLoad,
            hoursPerAnnum: hoursPerAnnum,
            runningHours: runningHours,
            consumption: consumption,
            reduction: reduction,
            percentageOfUsage: percentageOfUsage,
            electricityCostReduction: electricityCostReduction,
            maintenanceCostReduction: maintenanceCostReduction,
            totalSaving: totalSaving,
            electricityCost: electricityCost,
            maintenanceCost: maintenanceCost,
            costOfUpgrade: costOfUpgrade,
            paybackTime: paybackTime,
            greenBusiness: self.greenBusiness,
            greenBusinessContribution: greenBusinessContribution,
            greenBusinessReducedUpgradeCost: greenBusinessReducedUpgradeCost,
            greenBusinessPaybackTime: greenBusinessPaybackTime,
            co2Reduction: co2Reduction,
        };
    }
}); })
    .views(function (self) { return ({
    get newLightsWithProposedAdditionalControls() {
        var current = self.currentLightsWithExistingControls;
        var newExisting = self.newLightsWithExistingControls;
        var maxLoad = self.lights.reduce(function (load, l) { return load + l.data.led.effectiveLoad; }, 0);
        var hoursPerAnnum = self.newLightsWithExistingControls.hoursPerAnnum;
        var consumption = self.lights.reduce(function (load, l) { return load + l.data.control.annualLoadWithAdditionalControls; }, 0);
        var runningHours = consumption / maxLoad;
        var electricityCost = self.lights.reduce(function (load, l) { return load + l.data.control.reducedAnnualElectricityCost; }, 0);
        var reduction = current.consumption - consumption;
        var maintenanceCost = self.lights.reduce(function (load, l) { return load + l.data.control.revisedMaintenanceCost; }, 0);
        var percentageOfUsage = reduction / current.consumption;
        var electricityCostReduction = current.electricityCost - electricityCost;
        var maintenanceCostReduction = current.maintenanceCost - maintenanceCost;
        var totalSaving = electricityCostReduction + maintenanceCostReduction;
        var costOfUpgrade = self.lights.reduce(function (load, l) { return load + l.data.control.increaseCostOfUpgrade; }, 0) + newExisting.costOfUpgrade;
        var paybackTime = costOfUpgrade / totalSaving;
        var greenBusinessContribution = (self.greenBusiness && costOfUpgrade > 5000) ? Math.min(5000, costOfUpgrade * 0.15 * Math.min(1, 5 / (costOfUpgrade / totalSaving))) : 0;
        var greenBusinessReducedUpgradeCost = self.greenBusiness ? costOfUpgrade - greenBusinessContribution : 0;
        var greenBusinessPaybackTime = self.greenBusiness ? greenBusinessReducedUpgradeCost / totalSaving : 0;
        var co2Reduction = self.lights.reduce(function (load, l) { return load + l.data.control.totalCo2Reduction; }, 0);
        return {
            maxLoad: maxLoad,
            hoursPerAnnum: hoursPerAnnum,
            runningHours: runningHours,
            consumption: consumption,
            reduction: reduction,
            percentageOfUsage: percentageOfUsage,
            electricityCostReduction: electricityCostReduction,
            maintenanceCostReduction: maintenanceCostReduction,
            totalSaving: totalSaving,
            electricityCost: electricityCost,
            maintenanceCost: maintenanceCost,
            costOfUpgrade: costOfUpgrade,
            paybackTime: paybackTime,
            greenBusiness: self.greenBusiness,
            greenBusinessContribution: greenBusinessContribution,
            greenBusinessReducedUpgradeCost: greenBusinessReducedUpgradeCost,
            greenBusinessPaybackTime: greenBusinessPaybackTime,
            co2Reduction: co2Reduction,
        };
    }
}); })
    .views(function (self) { return ({
    get summary() {
        var newLightsWithExistingControls = self.newLightsWithExistingControls, currentLightsWithExistingControls = self.currentLightsWithExistingControls, newLightsWithProposedAdditionalControls = self.newLightsWithProposedAdditionalControls;
        var data = {
            currentLightsWithExistingControls: JSON.parse(JSON.stringify(currentLightsWithExistingControls)),
            newLightsWithExistingControls: JSON.parse(JSON.stringify(newLightsWithExistingControls)),
            newLightsWithProposedAdditionalControls: JSON.parse(JSON.stringify(newLightsWithProposedAdditionalControls))
        };
        return data;
    }
}); })
    .actions(function (self) {
    function addLight(light) {
        if (typeof light === 'string') {
            var sourceLight = self.availableLights.find(function (l) { return l.id === light; });
            if (!sourceLight)
                return;
            light = sourceLight.clone();
        }
        light.setOccupancyHoursPerAnnun(self.occupancyHours);
        light.setElectricityPrice(self.electricityPrice);
        light.setCo2Emissions(self.co2Emissions);
        self.lights.unshift(light);
    }
    function removeLight(light) {
        var index = self.lights.indexOf(light);
        self.lights.splice(index, 1);
    }
    function setElectricityPrice(price) {
        self.electricityPrice = price;
        self.lights.forEach(function (l) { return l.setElectricityPrice(price); });
    }
    function setOccupancyHours(hours) {
        self.occupancyHours = hours;
        self.lights.forEach(function (l) { return l.setOccupancyHoursPerAnnun(hours); });
    }
    function setCo2Emissions(emissions) {
        self.co2Emissions = emissions;
        self.lights.forEach(function (l) { return l.setCo2Emissions(emissions); });
    }
    function setGreenBusiness(greenBusiness) {
        self.greenBusiness = greenBusiness;
    }
    return { addLight: addLight, removeLight: removeLight, setElectricityPrice: setElectricityPrice, setOccupancyHours: setOccupancyHours, setCo2Emissions: setCo2Emissions, setGreenBusiness: setGreenBusiness };
});
var Usage = (function (_super) {
    __extends(Usage, _super);
    function Usage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Usage.prototype.addLight = function (light) {
        this.props.store.addLight(light);
    };
    Object.defineProperty(Usage.prototype, "lights", {
        get: function () {
            var _this = this;
            return this.props.store.lights.map(function (light) { return (React.createElement(Light.Light, { key: light.id.toString(), store: light, controls: _this.props.store.controls, onDelete: _this.props.store.removeLight })); });
        },
        enumerable: true,
        configurable: true
    });
    Usage.prototype.render = function () {
        var availableLights = this.props.store.availableLights;
        return (React.createElement("div", { style: { position: 'absolute', top: '15.1em', width: '100%', bottom: '3.1em', overflowY: 'auto' } },
            React.createElement(CardContent, { style: { background: '#f7f7f7', overflowY: 'auto', height: '100%' } },
                React.createElement(LightPicker, { available: availableLights, onAddLight: this.addLight }),
                React.createElement("div", { style: { marginTop: '1em' } }, this.lights))));
    };
    __decorate([
        action.bound
    ], Usage.prototype, "addLight", null);
    __decorate([
        computed
    ], Usage.prototype, "lights", null);
    Usage = __decorate([
        observer
    ], Usage);
    return Usage;
}(React.Component));
export { Usage };
var LightPicker = (function (_super) {
    __extends(LightPicker, _super);
    function LightPicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LightPicker.prototype.onAddLight = function (id) {
        var available = this.props.available;
        var lightStore = available.find(function (l) { return l.id.toString() === id; });
        if (!lightStore)
            return;
        this.props.onAddLight(lightStore.clone());
    };
    Object.defineProperty(LightPicker.prototype, "items", {
        get: function () {
            return this.props.available.map(function (_a) {
                var id = _a.id, label = _a.label, power = _a.power, size = _a.size;
                return ({
                    id: id.toString(),
                    label: label + " " + power + "W " + size
                });
            });
        },
        enumerable: true,
        configurable: true
    });
    LightPicker.prototype.render = function () {
        return React.createElement(Dropdown, { items: this.items, type: "picker", separator: true, label: "Add light", onChange: this.onAddLight, maxHeight: 240, fullWidth: true });
    };
    __decorate([
        action.bound
    ], LightPicker.prototype, "onAddLight", null);
    __decorate([
        computed
    ], LightPicker.prototype, "items", null);
    return LightPicker;
}(React.Component));
//# sourceMappingURL=Usage.js.map