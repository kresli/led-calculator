var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { action, computed } from 'mobx';
import { observer } from 'mobx-react';
import { Help } from 'bloomer';
import marked from 'marked';
import { types } from 'mobx-state-tree';
export var Store = types
    .model({
    id: types.identifier(),
    label: types.union(types.string, types.undefined),
    helpText: types.union(types.string, types.undefined),
    markdown: types.optional(types.boolean, false),
    checked: types.boolean,
})
    .actions(function (self) {
    function toggleChecked() {
        self.checked = !self.checked;
    }
    return { toggleChecked: toggleChecked };
});
var Checkbox = (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Checkbox.prototype.toggleChecked = function () {
        this.props.store.toggleChecked();
        this.props.onChange && this.props.onChange(this.props.store.checked);
    };
    Object.defineProperty(Checkbox.prototype, "helpText", {
        get: function () {
            var _a = this.props.store, helpText = _a.helpText, markdown = _a.markdown;
            if (!helpText)
                return null;
            return markdown && React.createElement(Help, { dangerouslySetInnerHTML: { __html: marked(helpText) } }) || React.createElement(Help, null, helpText);
        },
        enumerable: true,
        configurable: true
    });
    Checkbox.prototype.render = function () {
        var _a = this.props.store, label = _a.label, checked = _a.checked;
        return (React.createElement("div", { className: "field", onClick: this.toggleChecked },
            React.createElement("input", { type: "checkbox", name: "test", className: "switch", checked: checked, onChange: function () { } }),
            React.createElement("label", null, label),
            this.helpText));
    };
    __decorate([
        action.bound
    ], Checkbox.prototype, "toggleChecked", null);
    __decorate([
        computed
    ], Checkbox.prototype, "helpText", null);
    Checkbox = __decorate([
        observer
    ], Checkbox);
    return Checkbox;
}(React.Component));
export { Checkbox };
//# sourceMappingURL=Checkbox.js.map