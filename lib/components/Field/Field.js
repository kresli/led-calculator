var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import { observer } from 'mobx-react';
import { types } from 'mobx-state-tree';
import { observe } from 'mobx';
import * as Input from './Input';
import * as Checkbox from './Checkbox';
import * as Stepper from './Stepper';
import uniqid from 'uniqid';
var Field = (function (_super) {
    __extends(Field, _super);
    function Field() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Field.prototype.render = function () {
        var _a = this.props.store, type = _a.type, typeStore = _a.typeStore;
        switch (type) {
            case 'input': return typeStore && React.createElement(Input.Input, { store: typeStore }) || React.createElement("div", null, "shite");
            case 'checkbox': return React.createElement(Checkbox.Checkbox, { store: typeStore });
            case 'stepper': return React.createElement(Stepper.Stepper, null);
            default: return null;
        }
    };
    Field = __decorate([
        observer
    ], Field);
    return Field;
}(React.Component));
export { Field };
var typeStores = new Map();
export var Store = types
    .model('Field', {
    id: types.optional(types.identifier(types.string), uniqid()),
    type: types.enumeration(['checkbox', 'input', 'stepper']),
    value: types.union(types.boolean, types.string, types.number),
    placeholder: types.optional(types.union(types.string, types.undefined), undefined),
    mandatory: types.optional(types.boolean, false),
    label: types.optional(types.union(types.string, types.undefined), undefined),
    showIfChecked: types.optional(types.array(types.string), []),
    suffix: types.optional(types.union(types.string, types.undefined), undefined),
    helpText: types.optional(types.union(types.string, types.undefined), undefined),
    leftIcon: types.optional(types.union(types.string, types.undefined), undefined),
    rightIcon: types.optional(types.union(types.string, types.undefined), undefined),
    maxLength: types.optional(types.union(types.number, types.undefined), undefined),
    minLength: types.optional(types.union(types.number, types.undefined), undefined),
    format: types.optional(types.union(types.enumeration(['number', 'string']), types.undefined), undefined),
    markdown: types.optional(types.boolean, false),
})
    .views(function (self) { return ({
    get data() {
        var id = self.id, type = self.type, value = self.value, mandatory = self.mandatory, format = self.format, label = self.label;
        return { id: id, type: type, value: value, mandatory: mandatory, format: format, label: label };
    },
    get typeStore() {
        return typeStores.get(self);
    },
    get valid() {
        return typeStores.get(self).valid;
    }
}); })
    .actions(function (self) {
    function setValue(value) {
        self.value = value;
    }
    return { setValue: setValue };
})
    .actions(function (self) {
    function beforeDestroy() {
        typeStores.delete(self);
    }
    function afterCreate() {
        var id = self.id, type = self.type, value = self.value, placeholder = self.placeholder, mandatory = self.mandatory, label = self.label, suffix = self.suffix, helpText = self.helpText, leftIcon = self.leftIcon, rightIcon = self.rightIcon, maxLength = self.maxLength, minLength = self.minLength, format = self.format, markdown = self.markdown;
        var typeStore;
        switch (type) {
            case 'input':
                typeStore = Input.Store.create({ id: id, value: value, placeholder: placeholder, mandatory: mandatory, label: label, suffix: suffix, helpText: helpText, leftIcon: leftIcon, rightIcon: rightIcon, maxLength: maxLength, minLength: minLength, format: format, markdown: markdown });
                observe(typeStore, 'value', function (change) { return self.setValue(change.newValue.storedValue); });
                break;
            case 'checkbox':
                typeStore = Checkbox.Store.create({ id: id, markdown: markdown, checked: value, label: label, helpText: helpText });
                observe(typeStore, 'checked', function (change) { return self.setValue(change.newValue.storedValue); });
                break;
            case 'stepper':
                typeStore = Stepper.Store.create({ value: value });
                observe(typeStore, 'value', function (change) { return self.setValue(change.newValue.storedValue); });
                break;
            default: throw (self.type + " is not valid type for Field component");
        }
        typeStores.set(self, typeStore);
    }
    return { afterCreate: afterCreate, beforeDestroy: beforeDestroy };
});
//# sourceMappingURL=Field.js.map