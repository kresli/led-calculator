var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as storybook from '@storybook/react';
import * as React from 'react';
import * as Field from './Field';
import { withKnobs } from '@storybook/addon-knobs';
import { Message, MessageBody, MessageHeader } from 'bloomer';
import { observer } from 'mobx-react';
import { action, observable } from 'mobx';
import { boolean, text, select, array, number } from '@storybook/addon-knobs';
var stories = storybook
    .storiesOf('Field', module)
    .addDecorator(withKnobs);
function storyPadding(story) {
    return (React.createElement("div", { style: { padding: '12px' } }, story));
}
function makeCheckboxKnobs(knobsShared) {
    return __assign({}, knobsShared, {
        value: boolean('value?(checkbox)', false),
        defaultValue: boolean('defaultValue?(checkbox)', false),
    });
}
function makeInputKnobs(knobsShared) {
    return __assign({}, knobsShared, {
        value: text('value?', ''),
        suffix: text('suffix?', ''),
        defaultValue: text('defaultValue?', ''),
        placeholder: text('placeholder?', ''),
        mandatory: boolean('mandatory?', false),
        leftIcon: text('leftIcon?', ''),
        rightIcon: text('rightIcon?', ''),
        minLength: number('minLength?', 0),
        maxLength: number('minLength?', 0),
        format: select('format?', ['string', 'number'], 'string'),
    });
}
;
stories.add('playground', function () {
    var knobsShared = {
        id: text('id', 'inputId'),
        type: select('type', { 'input': 'input', 'checkbox': 'checkbox' }, 'input'),
        helpText: text('helpText?', ''),
        showIfChecked: array('showIfChecked?', []),
        label: text('label?', ''),
        markdown: boolean('markdown?', false),
    };
    var knobsTypes = {
        input: makeInputKnobs,
        checkbox: makeCheckboxKnobs,
    };
    var store = Field.Store.create(knobsTypes[knobsShared.type](knobsShared));
    return storyPadding(React.createElement(Field.Field, { store: store }));
});
stories.add('showcase', function () {
    var FieldStory = (function (_super) {
        __extends(FieldStory, _super);
        function FieldStory() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.storeToJSON = '';
            return _this;
        }
        FieldStory.prototype.onInput = function () {
            var store = this.props.store;
            this.storeToJSON = JSON.stringify(store, null, '  ');
        };
        FieldStory.prototype.render = function () {
            var store = this.props.store;
            return (React.createElement("div", null,
                React.createElement(Field.Field, { store: Field.Store.create(store) }),
                React.createElement(Message, null,
                    React.createElement(MessageHeader, null,
                        React.createElement("p", null, "store")),
                    React.createElement(MessageBody, null,
                        React.createElement("pre", null, JSON.stringify(store, null, '  '))))));
        };
        __decorate([
            observable
        ], FieldStory.prototype, "storeToJSON", void 0);
        __decorate([
            action.bound
        ], FieldStory.prototype, "onInput", null);
        FieldStory = __decorate([
            observer
        ], FieldStory);
        return FieldStory;
    }(React.Component));
    function makeFieldStore(data) {
        var defaults = {
            id: undefined,
            type: undefined,
            value: undefined,
            placeholder: undefined,
            mandatory: undefined,
            label: undefined,
            showIfChecked: [],
            suffix: undefined,
            helpText: undefined,
            leftIcon: undefined,
            rightIcon: undefined,
            maxLength: undefined,
            minLength: undefined,
            format: undefined,
            markdown: undefined,
        };
        return Field.Store.create(__assign({}, defaults, data));
    }
    function createField(label, data) {
        return (React.createElement("div", null,
            React.createElement("br", null),
            React.createElement("h3", { className: "title has-text-centered" }, label),
            React.createElement(FieldStory, { store: Field.Store.create(data) })));
    }
    return (React.createElement("div", { style: { padding: '12px' } },
        createField('simple', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
        })),
        createField('placeholder', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            placeholder: 'Placeholder'
        })),
        createField('mandatory', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            mandatory: true,
        })),
        createField('label', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            label: 'Label',
        })),
        createField('help text', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            helpText: 'Help Text',
        })),
        createField('default value', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            defaultValue: 'Default Value'
        })),
        createField('left icon', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            leftIcon: 'home'
        })),
        createField('right icon', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '',
            rightIcon: 'home',
        })),
        createField('value', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: 'Value',
        })),
        createField('min char length 3', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '12',
            minLength: 3,
        })),
        createField('max char length 3', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '1234',
            maxLength: 3,
        })),
        createField('format string', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: '123',
            format: 'string',
        })),
        createField('format number', makeFieldStore({
            id: 'myInput',
            type: 'input',
            value: 'string',
            format: 'number'
        })),
        createField('simple checkbox', makeFieldStore({
            id: 'myCheckbox',
            type: 'checkbox',
            value: true,
        })),
        createField('labeled checkbox', makeFieldStore({
            id: 'myCheckbox',
            type: 'checkbox',
            value: true,
            label: 'Builder'
        }))));
});
//# sourceMappingURL=Field.story.js.map