var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import React from 'react';
import { Input as BloomerInput, Field as BloomerField, Label, Control, Icon, Help, Button } from 'bloomer';
import { observer } from 'mobx-react';
import marked from 'marked';
import { types } from 'mobx-state-tree';
var Input = (function (_super) {
    __extends(Input, _super);
    function Input() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Input.prototype.render = function () {
        var store = this.props.store;
        var leftIcon = store.leftIcon, rightIcon = store.rightIcon, suffix = store.suffix, label = store.label, helpText = store.helpText, markdown = store.markdown;
        return (React.createElement(BloomerField, { hasAddons: true, style: { flexDirection: 'column' } },
            React.createElement(InputLabel, { label: label }),
            React.createElement("div", { style: { flexDirection: 'row', display: 'flex' } },
                React.createElement(Control, { hasIcons: (leftIcon || rightIcon) && true || false, isExpanded: true },
                    React.createElement(InputArea, { store: store }),
                    React.createElement(InputIcon, { icon: leftIcon, isAlign: "left" }),
                    React.createElement(InputIcon, { icon: rightIcon, isAlign: "right" }),
                    React.createElement(Required, { store: this.props.store })),
                React.createElement(Suffix, { suffix: suffix })),
            React.createElement(HelpText, { helpText: helpText, markdown: markdown })));
    };
    return Input;
}(React.Component));
export { Input };
export var Store = types
    .model('Input', {
    id: types.identifier(),
    label: types.union(types.string, types.undefined),
    placeholder: types.union(types.string, types.undefined),
    helpText: types.union(types.string, types.undefined),
    minLength: types.optional(types.number, 0),
    leftIcon: types.union(types.string, types.undefined),
    rightIcon: types.union(types.string, types.undefined),
    suffix: types.union(types.string, types.undefined),
    value: types.string,
    maxLength: types.union(types.number, types.undefined),
    format: types.optional(types.enumeration(['number', 'string']), 'string'),
    mandatory: types.optional(types.boolean, false),
    markdown: types.boolean,
})
    .views(function (self) { return ({
    get errorMessage() {
        var value = self.value, format = self.format, mandatory = self.mandatory;
        if (mandatory && self.value.length === 0)
            return 'This field is required';
        switch (format) {
            case 'number': return /^[0-9]*$/.test(value) ? null : 'Expected value to be number';
            case 'string': return /.*/.test(value) ? null : 'Expected value to be text';
            default: return "Format " + format + " is not registered";
        }
    },
    get valid() {
        return !this.errorMessage;
    }
}); })
    .actions(function (self) {
    function setValue(value) {
        self.value = value;
    }
    return { setValue: setValue };
});
var InputArea = observer(function (_a) {
    var store = _a.store;
    var valid = store.valid, placeholder = store.placeholder, value = store.value;
    var color = valid ? undefined : 'danger';
    return (React.createElement(BloomerInput, { isColor: color, placeholder: placeholder, value: value, onInput: function (ev) { return store.setValue(ev.currentTarget.value); }, onChange: function () { } }));
});
var HelpText = observer(function (_a) {
    var helpText = _a.helpText, markdown = _a.markdown;
    if (!helpText)
        return null;
    if (markdown) {
        return React.createElement(Control, null,
            " ",
            React.createElement(Help, { dangerouslySetInnerHTML: { __html: marked(helpText) } }),
            " ");
    }
    else {
        return React.createElement(Help, null, helpText);
    }
});
var InputLabel = observer(function (_a) {
    var label = _a.label;
    return label && React.createElement(Label, null, label) || null;
});
var Suffix = function (_a) {
    var suffix = _a.suffix;
    if (!suffix)
        return null;
    return (React.createElement(Control, null,
        React.createElement(Button, { isStatic: true }, suffix)));
};
var Required = observer(function (_a) {
    var store = _a.store;
    var valid = store.valid, errorMessage = store.errorMessage;
    if (valid)
        return null;
    return (React.createElement("div", null, React.createElement(Help, { isColor: "danger" }, errorMessage)));
});
var InputIcon = observer(function (props) {
    return props.icon && React.createElement(Icon, __assign({ isSize: "small" }, props)) || null;
});
//# sourceMappingURL=Input.js.map