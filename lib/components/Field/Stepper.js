var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import React from 'react';
import { types } from 'mobx-state-tree';
export var Store = types.model('Stepper')
    .props({
    value: types.number,
});
var Stepper = (function (_super) {
    __extends(Stepper, _super);
    function Stepper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Stepper.prototype.render = function () {
        return React.createElement("div", null, "stepper");
    };
    return Stepper;
}(React.Component));
export { Stepper };
//# sourceMappingURL=Stepper.js.map