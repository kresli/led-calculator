var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import React from 'react';
import * as Field from '..//Field';
import { computed, action } from 'mobx';
import { observer } from 'mobx-react';
var Form = (function (_super) {
    __extends(Form, _super);
    function Form() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Form.prototype.onInput = function (ev) {
        // this.props.store.onInput();
        console.log(ev);
        if (this.props.onInput)
            this.props.onInput(ev);
    };
    Object.defineProperty(Form.prototype, "fields", {
        get: function () {
            return this.props.store.visibleFields.map(function (store) { return (React.createElement(Field.Field, { key: store.id, store: store })); });
        },
        enumerable: true,
        configurable: true
    });
    Form.prototype.render = function () {
        return React.createElement("div", null, this.fields);
    };
    __decorate([
        action.bound
    ], Form.prototype, "onInput", null);
    __decorate([
        computed
    ], Form.prototype, "fields", null);
    Form = __decorate([
        observer
    ], Form);
    return Form;
}(React.Component));
export { Form };
//# sourceMappingURL=Form.js.map