var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as storybook from '@storybook/react';
import * as React from 'react';
import * as Form from './';
import { withKnobs } from '@storybook/addon-knobs';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Message, MessageHeader, MessageBody } from 'bloomer';
var stories = storybook
    .storiesOf('Form', module)
    .addDecorator(withKnobs);
stories.add('data', function () {
    var Output = (function (_super) {
        __extends(Output, _super);
        function Output() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Output.prototype.render = function () {
            return (React.createElement("div", { style: { padding: '12px' } },
                React.createElement(Form.Form, { store: this.props.store }),
                React.createElement("br", null),
                React.createElement(Message, null,
                    React.createElement(MessageHeader, null,
                        React.createElement("p", null, "data")),
                    React.createElement(MessageBody, null,
                        React.createElement("pre", null, JSON.stringify(toJS(this.props.store.data), null, '  '))))));
        };
        Output = __decorate([
            observer
        ], Output);
        return Output;
    }(React.Component));
    var fields = [
        {
            id: 'Simple input',
            type: 'input',
            value: '',
            label: 'Name'
        },
        {
            id: 'showMandatoryInput',
            type: 'checkbox',
            value: false,
            label: 'Show mandatory Input'
        },
        {
            id: 'hiddenMandatoryInput',
            label: 'Mandatory hidden input',
            type: 'input',
            value: '',
            showIfChecked: ['showMandatoryInput'],
        }
    ];
    var store = Form.Store.create({ fields: fields });
    return React.createElement(Output, { store: store });
});
//# sourceMappingURL=Form.story.js.map