import { types } from 'mobx-state-tree';
import * as Field from '../Field';
export var Store = types
    .model('Form', {
    fields: types.optional(types.array(Field.Store), []),
})
    .views(function (self) { return ({
    get visibleFields() {
        return self.fields.filter(function (field) {
            if (!field.showIfChecked || field.showIfChecked.length === 0)
                return field;
            return field.showIfChecked.every(function (fieldId) {
                var checkField = self.fields.find(function (f) { return f.id === fieldId; });
                if (!checkField || checkField.type !== 'checkbox')
                    return true;
                return checkField.value && true || false;
            });
        });
    },
}); })
    .views(function (self) { return ({
    get validationFields() {
        return self.visibleFields.filter(function (field) { return field.valid !== undefined; });
    }
}); })
    .views(function (self) { return ({
    get valid() {
        return self.validationFields.every(function (field) { return field.valid === true; });
    },
}); })
    .views(function (self) { return ({
    get data() {
        return {
            fields: self.visibleFields.map(function (field) {
                var _a = field.data, value = _a.value, label = _a.label, id = _a.id, mandatory = _a.mandatory, type = _a.type, format = _a.format;
                return { value: value, label: label, id: id, mandatory: mandatory, type: type, format: format };
            })
        };
    }
}); });
//# sourceMappingURL=Store.js.map