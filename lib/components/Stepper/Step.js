var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from 'react';
import classNames from 'classnames';
import { action, computed } from 'mobx';
import { observer } from 'mobx-react';
import { types } from 'mobx-state-tree';
export var Store = types
    .model('StepperStep', {
    id: types.identifier(types.string),
    label: types.string,
    active: types.optional(types.boolean, false),
    disabled: types.optional(types.boolean, false)
})
    .actions(function (self) {
    function setActive(active) {
        self.active = active;
    }
    function setDisabled(disabled) {
        self.disabled = disabled;
    }
    return { setActive: setActive, setDisabled: setDisabled };
});
var Step = (function (_super) {
    __extends(Step, _super);
    function Step() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Step.prototype, "className", {
        get: function () {
            return classNames({
                'steps-segment': true,
                'is-active': this.props.store.active,
            });
        },
        enumerable: true,
        configurable: true
    });
    Step.prototype.onClick = function () {
        this.props.onClick(this.props.store);
    };
    Object.defineProperty(Step.prototype, "marker", {
        get: function () {
            var props = {
                className: 'steps-marker',
                onClick: this.props.store.disabled ? undefined : this.onClick,
            };
            return React.createElement("a", __assign({}, props));
        },
        enumerable: true,
        configurable: true
    });
    Step.prototype.render = function () {
        return (React.createElement("li", { className: this.className },
            this.marker,
            React.createElement("div", { className: "steps-content" },
                React.createElement("p", { className: "is-size-7" }, this.props.store.label))));
    };
    __decorate([
        computed
    ], Step.prototype, "className", null);
    __decorate([
        action.bound
    ], Step.prototype, "onClick", null);
    __decorate([
        computed
    ], Step.prototype, "marker", null);
    Step = __decorate([
        observer
    ], Step);
    return Step;
}(React.Component));
export { Step };
//# sourceMappingURL=Step.js.map