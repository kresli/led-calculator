var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from 'react';
import { observer } from 'mobx-react';
import { action, computed } from 'mobx';
import * as Step from './Step';
import { types } from 'mobx-state-tree';
import uniqid from 'uniqid';
export var Store = types
    .model('Stepper', {
    id: types.optional(types.identifier(), uniqid()),
    steps: types.array(Step.Store),
})
    .views(function (self) { return ({
    get activeStep() {
        return self.steps.find(function (s) { return s.active === true; }) || self.steps[0];
    },
    get activeStepIndex() {
        return self.steps.indexOf(this.activeStep);
    },
    get activeStepId() {
        return this.activeStep.id;
    }
}); })
    .actions(function (self) {
    function setActiveStep(step) {
        var activeStep;
        if (typeof step === 'string') {
            activeStep = self.steps.find(function (s) { return s.id === step; });
            if (!activeStep)
                return;
        }
        else {
            activeStep = step;
        }
        self.steps.forEach(function (s) { return s.setActive(false); });
        activeStep.active = true;
    }
    function nextStep() {
        var stepIndex = self.activeStepIndex + 1;
        if (stepIndex > self.steps.length - 1)
            return;
        self.steps.forEach(function (step) { return step.active = false; });
        self.steps[stepIndex].active = true;
    }
    function previousStep() {
        var stepIndex = self.activeStepIndex - 1;
        if (stepIndex < 0)
            return;
        self.steps.forEach(function (step) { return step.active = false; });
        self.steps[stepIndex].active = true;
    }
    function disableAfterActiveStep(disabled) {
        for (var index = self.activeStepIndex + 1; index < self.steps.length; index++) {
            self.steps[index].setDisabled(disabled);
        }
    }
    return { setActiveStep: setActiveStep, nextStep: nextStep, previousStep: previousStep, disableAfterActiveStep: disableAfterActiveStep };
});
var Stepper = (function (_super) {
    __extends(Stepper, _super);
    function Stepper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Stepper.prototype.onClick = function (step) {
        this.props.store.setActiveStep(step);
    };
    Object.defineProperty(Stepper.prototype, "steps", {
        get: function () {
            var _this = this;
            return this.props.store.steps.map(function (step, index) {
                return (React.createElement(Step.Step, { key: step.id, store: step, onClick: _this.onClick }));
            });
        },
        enumerable: true,
        configurable: true
    });
    Stepper.prototype.render = function () {
        return (React.createElement("ul", { className: "steps is-horizontal has-content-centered", style: { marginTop: '1em' } }, this.steps));
    };
    __decorate([
        action.bound
    ], Stepper.prototype, "onClick", null);
    __decorate([
        computed
    ], Stepper.prototype, "steps", null);
    Stepper = __decorate([
        observer
    ], Stepper);
    return Stepper;
}(React.Component));
export { Stepper };
//# sourceMappingURL=Stepper.js.map