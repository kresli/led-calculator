import * as storybook from '@storybook/react';
import React from 'react';
import * as Stepper from './Stepper';
var stories = storybook.storiesOf('Stepper', module);
function storyPadding(story) {
    return (React.createElement("div", { style: { padding: '12px' } }, story));
}
stories.add('default', function () {
    var store = Stepper.Store.create({
        steps: [
            {
                id: 'first',
                label: 'First',
            },
            {
                id: 'second',
                label: 'second',
            },
            {
                id: 'third',
                label: 'Third',
            },
            {
                id: 'fourth',
                label: 'Fourth',
            }
        ],
    });
    return storyPadding(React.createElement(Stepper.Stepper, { store: store }));
});
stories.add('third step selected', function () {
    var store = Stepper.Store.create({
        steps: [
            {
                id: 'first',
                label: 'First',
            },
            {
                id: 'second',
                label: 'second',
            },
            {
                id: 'third',
                label: 'Third',
            },
            {
                id: 'fourth',
                label: 'Fourth',
            }
        ]
    });
    return storyPadding(React.createElement(Stepper.Stepper, { store: store }));
});
//# sourceMappingURL=Stepper.story.js.map