var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import React from 'react';
import { IntlProvider } from 'react-intl';
import { observer } from 'mobx-react';
import hoistNonReactStatic from 'hoist-non-react-statics';
/* tslint:disable:no-any */
export function publicComponent(Comp) {
    var PublicComponent = (function (_super) {
        __extends(PublicComponent, _super);
        function PublicComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        PublicComponent.prototype.render = function () {
            return (React.createElement(IntlProvider, { locale: this.props.locale },
                React.createElement(Comp, __assign({}, this.props))));
        };
        return PublicComponent;
    }(React.Component));
    hoistNonReactStatic(PublicComponent, Comp);
    return observer(PublicComponent);
}
/*tslint:disable:no-any */
export function ui(Comp) {
    var UiComponent = (function (_super) {
        __extends(UiComponent, _super);
        function UiComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        UiComponent.prototype.render = function () {
            return (React.createElement("div", { className: "rocket" },
                React.createElement(Comp, __assign({}, this.props))));
        };
        return UiComponent;
    }(React.Component));
    hoistNonReactStatic(UiComponent, Comp);
    return observer(UiComponent);
}
//# sourceMappingURL=decorators.js.map