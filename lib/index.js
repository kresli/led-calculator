import './index.css';
import * as Calculator from './components/Calculator/Calculator';
import React from 'react';
import ReactDOM from 'react-dom';
console.log('script loaded');
document.addEventListener('DOMContentLoaded', function () {
    var roots = document.querySelectorAll("div[rocket-component*=\"Calculator\"]");
    console.log('domContentLoaded', roots);
    Array.from(roots).forEach(function (root) {
        var snapshot = window.calculatorData;
        var store = Calculator.Store.create(snapshot);
        ReactDOM.render((React.createElement(Calculator.Calculator, { store: store })), root);
    });
});
//# sourceMappingURL=index.js.map