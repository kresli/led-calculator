import React from 'react';
import { action, computed, observe } from 'mobx';
import './style.css';
import { observer } from 'mobx-react';
import { Hero, Container, Title, Subtitle, Card, CardImage, CardFooter, CardFooterItem } from 'bloomer';
import * as Consumption from './steps/Consumption/Consumption';
import * as Usage from './steps/Usage/Usage';
import * as Results from './steps/Results/Results';
// import * as Stepper from '../../modules/Stepper';
import { types as t } from 'mobx-state-tree';
import * as Stepper from '../Stepper';
import classNames from 'classnames';
import { LightSummary } from './steps/Usage/Usage';

export interface Props {
  store: typeof Store.Type;
}

const localStore = {
  stepper: new Map(),
};

export const Store = t
  .model('Calculator', {
    title: t.string,
    subtitle: t.string,
    height: t.optional(t.string, 'auto'),
    consumption: Consumption.Store,
    usage: Usage.Store,
    results: Results.Store,
    activeStep: t.string,
    co2Emissions: t.number,
  })
  .views( self => {
    return {
      get backButtonVisible(): boolean {
        const stepper = localStore.stepper.get(self);
        return self.activeStep !== stepper.steps[0].id;
      },
      get nextButtonVisible(): boolean {
        const stepper = localStore.stepper.get(self);
        const {steps} = stepper;
        return self.activeStep !== steps[steps.length - 1].id;
      },
      get stepper(): typeof Stepper.Store.Type {
        return localStore.stepper.get(self);
      },
      get activeStepValid(): boolean {
        switch ( self.activeStep ) {
          case 'consumption': return self.consumption.valid;
          default: return true;
        }
      },
      get summary(): LightSummary {
        return self.usage.summary
      },
      get prettySummary(): LightSummary {
        const current = self.usage.summary.currentLightsWithExistingControls;
        const existing = self.usage.summary.newLightsWithExistingControls;
        const proposed = self.usage.summary.newLightsWithProposedAdditionalControls;
        return {
          currentLightsWithExistingControls: {
            maxLoad: parseFloat(current.maxLoad.toFixed(1)),
            hoursPerAnnum: parseInt(current.hoursPerAnnum.toFixed(0)),
            runningHours: parseInt(current.runningHours.toFixed(0)),
            consumption: parseInt(current.consumption.toFixed(0)),
            electricityCost: parseInt(current.electricityCost.toFixed(0)),
            maintenanceCost: parseInt(current.maintenanceCost.toFixed(0)),
            totalRunningCost: parseInt(current.totalRunningCost.toFixed(0)),
          },
          newLightsWithExistingControls: {
            maxLoad: parseFloat(existing.maxLoad.toFixed(1)),
            hoursPerAnnum: parseInt(existing.hoursPerAnnum.toFixed(0)),
            runningHours: parseInt(existing.runningHours.toFixed(0)),
            consumption: parseInt(existing.consumption.toFixed(0)),
            electricityCost: parseInt(existing.electricityCost.toFixed(0)),
            reduction: parseInt(existing.reduction.toFixed(0)),
            percentageOfUsage: parseInt( new Number(existing.percentageOfUsage * 100).toFixed(0)),
            electricityCostReduction: parseInt(existing.electricityCostReduction.toFixed(0)),
            maintenanceCostReduction: parseInt(existing.maintenanceCostReduction.toFixed(0)),
            maintenanceCost: parseInt(existing.maintenanceCost.toFixed(0)),
            totalSaving: parseInt(existing.totalSaving.toFixed(0)),
            costOfUpgrade: parseInt(existing.totalSaving.toFixed(0)),
            paybackTime: parseFloat(existing.paybackTime.toFixed(1)),
            greenBusiness: existing.greenBusiness,
            greenBusinessContribution: parseInt(existing.greenBusinessContribution.toFixed(0)),
            greenBusinessReducedUpgradeCost: parseInt(existing.greenBusinessReducedUpgradeCost.toFixed(0)),
            greenBusinessPaybackTime: parseFloat(existing.greenBusinessPaybackTime.toFixed(1)),
            co2Reduction: parseFloat(existing.co2Reduction.toFixed(2)),
          },
          newLightsWithProposedAdditionalControls: {
            maxLoad: parseFloat(proposed.maxLoad.toFixed(1)),
            hoursPerAnnum: parseInt(proposed.hoursPerAnnum.toFixed(0)),
            runningHours: parseInt(proposed.runningHours.toFixed(0)),
            consumption: parseInt(proposed.consumption.toFixed(0)),
            electricityCost: parseInt(proposed.electricityCost.toFixed(0)),
            reduction: parseInt(proposed.reduction.toFixed(0)),
            percentageOfUsage: parseInt(new Number(proposed.percentageOfUsage * 100).toFixed(0)),
            electricityCostReduction: parseInt(proposed.electricityCostReduction.toFixed(0)),
            maintenanceCostReduction: parseInt(proposed.maintenanceCostReduction.toFixed(0)),
            maintenanceCost: parseInt(proposed.maintenanceCost.toFixed(0)),
            totalSaving: parseInt(proposed.totalSaving.toFixed(0)),
            costOfUpgrade: parseInt(proposed.totalSaving.toFixed(0)),
            paybackTime: parseFloat(proposed.paybackTime.toFixed(1)),
            greenBusiness: proposed.greenBusiness,
            greenBusinessContribution: parseInt(proposed.greenBusinessContribution.toFixed(0)),
            greenBusinessReducedUpgradeCost: parseInt(proposed.greenBusinessReducedUpgradeCost.toFixed(0)),
            greenBusinessPaybackTime: parseFloat(proposed.greenBusinessPaybackTime.toFixed(1)),
            co2Reduction: parseFloat(proposed.co2Reduction.toFixed(2)),
          },
        }
      }
    };
  })
  .actions( self => {
    function setActiveStep(activeStep: string) {
      const stepper = localStore.stepper.get(self);
      stepper.setActiveStep(activeStep);
      self.activeStep = activeStep;
    }
    return {setActiveStep};
  })
  .actions( self => {

    function updateUsageByConsumption(fields: {id: string, value: string}[]){
      const newAverageElectricityPriceField = fields.find( (a: {id: string}) => a.id === 'averageElectricityPrice' ) || {value: ''};
      const newOccupancyHoursField = fields.find( (a: {id: string}) => a.id === 'occupancyHours' ) || {value: ''};
      const greenBusinessField = fields.find( (a: {id: string}) => a.id === 'greenBusiness') || {value: false};
      self.usage.setElectricityPrice(parseInt(newAverageElectricityPriceField.value, 10));
      self.usage.setOccupancyHours(parseInt(newOccupancyHoursField.value, 10));
      self.usage.setGreenBusiness(greenBusinessField.value as boolean);
      self.usage.setCo2Emissions(self.co2Emissions);
    }
    function nextStep() {
      const stepper = localStore.stepper.get(self);
      stepper.nextStep();
    }
    function previousStep() {
      const stepper = localStore.stepper.get(self);
      stepper.previousStep();
    }
    function afterCreate() {
      const {activeStep} = self;
      const stepper = Stepper.Store.create({
        steps: [
          {id: 'consumption', label: self.consumption.label, active: activeStep === 'consumption', disabled: true},
          {id: 'usage', label: self.usage.label, active: activeStep === 'usage', disabled: true},
          {id: 'results', label: self.results.label, active: activeStep === 'results', disabled: true},
        ]
      });
      observe(stepper, 'activeStepId', change => self.setActiveStep(change.newValue) );
      localStore.stepper.set(self, stepper)
      // lets sync consumpstion data with usage
      updateUsageByConsumption(self.consumption.data);
      observe(self.consumption, 'data',( {newValue} ) => {
        updateUsageByConsumption(newValue);
      }, true);
    }
    return {afterCreate, nextStep, previousStep};
  });

@observer
export class Calculator extends React.Component<Props> {

  @action.bound
  nextStep() {
    this.props.store.nextStep();
  }

  @action.bound
  previousStep() {
    this.props.store.previousStep();
  }

  @computed
  get stepper(): JSX.Element {
    console.log(this.props.store)
    return (
      <Stepper.Stepper store={this.props.store.stepper}/>
    );
  }

  @computed
  get content(): JSX.Element {
    const {consumption, activeStep, usage} = this.props.store;
    switch(activeStep) {
      case 'consumption': return <Consumption.Consumption store={consumption}/>;
      case 'usage': return <Usage.Usage store={usage}/>;
      default: return <Results.Results calculatorStore={this.props.store}/>;
    }
  }

  @computed
  get backButton(): JSX.Element | null {
    return this.props.store.backButtonVisible
      ? <CardFooterItem tag="a" onClick={this.previousStep}>Back</CardFooterItem>
      : null;
  }

  @computed
  get nextButton(): JSX.Element | null {
    const {nextButtonVisible, activeStepValid} = this.props.store;
    if (!nextButtonVisible) return null;

    const className = classNames({ 'button': !activeStepValid });
    const props = {
      className,
      tag: activeStepValid ? 'a' as 'a' : 'p' as 'p',
      disabled: activeStepValid ? undefined : true,
      onClick: activeStepValid ? this.nextStep : undefined
    };
    return <CardFooterItem {...props}>Next</CardFooterItem>
  }

  render() {
    const {title, subtitle, height} = this.props.store;

    return (
      <div className="rocket" style={{height}}>
        <Card style={{flex: 1, height: 'inherit'}}>
          <CardImage>
            <Hero isColor="primary">
              <div className="hero-body">
                <Container>
                  <Title isSize={4}>{title}</Title>
                  <Subtitle>
                    {subtitle}
                  </Subtitle>
                </Container>
              </div>
            </Hero>
          </CardImage>
          {this.stepper}
          {this.content}
          <CardFooter style={{position: 'absolute', bottom: 0, width: '100%'}}>
            {this.backButton}
            {this.nextButton}
          </CardFooter>
        </Card>
      </div>
    );
  }
}