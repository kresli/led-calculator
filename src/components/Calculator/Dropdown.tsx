import React from 'react';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import classNames from 'classnames';

interface Props {
  items: {id: string, label: string}[];
  selected?: string;
  label: string;
  onChange: (id: string) => void;
  type?: 'picker' | 'select';
  separator?: boolean;
  maxHeight?: number;
  fullWidth?: boolean;
}

@observer
export default class Dropdown extends React.Component<Props> {

  static defaultProps: Partial<Props> = {
    type: 'select' as 'select',
    separator: false,
    fullWidth: false,
  };

  @observable visibleMenu: boolean = false;

  @computed
  get rootClassName(): string {
    return classNames({
      'dropdown': true,
      'is-active': this.visibleMenu,
    });
  }

  @action.bound
  toggleVisibleMenu() {
    this.visibleMenu = !this.visibleMenu;
  }

  @action.bound
  hideVisibleMenu() {
    this.visibleMenu = false;
  }

  @action.bound
  onChange(id: string) {
    this.hideVisibleMenu();
    this.props.onChange(id);
  }

  @computed
  get menu() {
    const {items, maxHeight, separator, fullWidth} = this.props;
    const maxHeightCalculated = maxHeight ? `${maxHeight}px` : 'auto';
    const itemsElements: JSX.Element[] = [];
    const menuStyle = {
      width: fullWidth ? '100%' : 'auto',
    };

    items.forEach( (item, i) => {
      itemsElements.push((
        <a key={item.id} className="dropdown-item" onClick={() => this.onChange(item.id)}>
          {item.label}
        </a>
      ));
      if (separator) {
        itemsElements.push(( <hr key={i} className="dropdown-divider"/> ));
      }
    });

    return (
      <div className="dropdown-menu" id="dropdown-menu" role="menu" style={menuStyle}>
        <div className="dropdown-content" style={{maxHeight: maxHeightCalculated, overflow: 'auto' as 'auto'}}>
          {itemsElements}
        </div>
      </div>
    );
  }

  @computed
  get selectedLabel(): string {
    const {selected, items} = this.props;
    const selectedItem = items.find( i => i.id === selected );
    return !selectedItem ? '' : selectedItem.label;
  }

  @computed
  get label(): JSX.Element {
    const {type, label} = this.props;
    switch (type) {
      case 'select': return <span style={{flexWrap: 'wrap', justifyContent: 'center', display: 'flex'}}><strong>{this.selectedLabel}</strong><span style={{marginLeft: '1em'}}>{label}</span></span>;
      case 'picker': return <span>{label}</span>;
      default: return <div/>;
    }
  }

  @computed
  get rootStyle(): {} {
    const {fullWidth} = this.props;
    return {
      flex: 1,
      width: fullWidth ? '100%' : 'auto',
    };
  }

  render() {
    return (
      <div className={this.rootClassName} onMouseLeave={this.hideVisibleMenu} style={this.rootStyle}>
        <div className="dropdown-trigger" style={{flex: 1, display: 'flex'}}>
          <button
            style={{display: 'flex', height: 'auto', flex: 1}}
            className="button"
            aria-haspopup="true"
            aria-controls="dropdown-menu"
            onClick={this.toggleVisibleMenu}
          >
            {this.label}
            <span className="icon is-small">
              <i className="fa fa-angle-down" aria-hidden="true"/>
            </span>
          </button>
        </div>
        {this.menu}
      </div>
    )
  }
}