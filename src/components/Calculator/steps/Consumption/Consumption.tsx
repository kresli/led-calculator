import React from 'react';
import { observer } from 'mobx-react';
import * as Form from '../../../Form';
import { types, detach } from 'mobx-state-tree';
// import * as Stepper from '../../Stepper';
import { CardContent } from 'bloomer';
import { action } from 'mobx';
import * as Field from '../../../Field';

const forms = new Map();

export const Store = types
  .model('FormConsuptionStore', {
    label: types.string,
    fields: types.array(Field.Store),
  })
  .actions( self => {
    function afterCreate() {
      const form = Form.Store.create({
        fields: self.fields.map(s => detach(s)),
      });
      forms.set(self, form);
    }
    return {afterCreate};
  })
  .views( self => ({
    get form(): typeof Form.Store.Type {
      const form = forms.get(self);
      return form;
    },
    get valid(): boolean {
      const form = forms.get(self);
      return form.valid;
    },
  }))
  .views( self => ({
    get data(): {
      id: string,
      value: string,
      label: string,
    }[] {
      return self.form.fields.map( ({id, label, value}) => ({id, label: label as string, value: value as string}));
    }
  }));

interface Props {
  store: typeof Store.Type;
}

@observer
export class Consumption extends React.Component<Props> {

  componentWillMount(){
    this.checkValid();
  }

  @action.bound
  onInput() {
    this.checkValid();
  }

  checkValid() {
    // const {stepper, form} = this.props.store;
    // if (form.valid) {
    //   stepper.disableAfterActiveStep(false);
    // } else {
    //   stepper.disableAfterActiveStep(true);
    // }
  }

  render() {
    return (
      <CardContent>
        <Form.Form store={this.props.store.form} onInput={this.onInput}/>
      </CardContent>
    );
  }
}