import React from 'react';
import { types } from 'mobx-state-tree';
import { Store as CalculatorStore } from '../../Calculator';
import { CardContent, Table } from 'bloomer';
import { computed } from 'mobx';

interface Props {
  calculatorStore: typeof CalculatorStore.Type;
}

export const Store = types
  .model('ResultsStore')
  .props({
    label: types.string,
  });

const style = {
  table: {
    title: {
      textAlign: 'center',
      fontWeight: 'bold' as 'bold',
      background: '#02d0b2',
      color: 'white',
    }
  }
};

const Cell: React.SFC<{ value: string | number, prefix?: string, suffix?: string }> = ({value, prefix, suffix}) => (
  <td>
    {prefix && <small>{prefix}</small>}
    <strong>{value}</strong>
    {suffix && <small>{suffix}</small>}
  </td>
)

class SummaryOfSaving extends React.Component<Props> {

  @computed
  get greenBusiness(): JSX.Element[] | null {
    const {prettySummary} = this.props.calculatorStore;
    const existing = prettySummary.newLightsWithExistingControls;
    const proposed = prettySummary.newLightsWithProposedAdditionalControls;
    if( !proposed.greenBusiness ) return null;
    const title = (
      <tr>
        <td colSpan={4} style={style.table.title}>Using Carbon Trust Green Business Fund</td>
      </tr>
    );
    const contribution = (
      <tr title="pro-rated if payback > 5 years, maximum contribution £5,000, minimum project size £5,000">
        <td>15% contribution</td>
        <td/>
        <Cell value={existing.greenBusinessContribution} prefix="£"/>
        <Cell value={proposed.greenBusinessContribution} prefix="£"/>
      </tr>
    )
    const upgrade = (
      <tr>
        <td>Reduced upgrade cost</td>
        <td/>
        <Cell value={existing.greenBusinessReducedUpgradeCost} prefix="~£" suffix=" (excl. VAT)"/>
        <Cell value={proposed.greenBusinessReducedUpgradeCost} prefix="~£" suffix=" (excl. VAT)"/>
      </tr>
    )
    const payback = (
      <tr>
        <td>Payback time</td>
        <td/>
        <Cell value={existing.greenBusinessPaybackTime} suffix=" years"/>
        <Cell value={proposed.greenBusinessPaybackTime} suffix=" years"/>
      </tr>
    )
    return [title, contribution, upgrade, payback];
  }

  render(){

    const {prettySummary} = this.props.calculatorStore;
    const current = prettySummary.currentLightsWithExistingControls;
    const existing = prettySummary.newLightsWithExistingControls;
    const proposed = prettySummary.newLightsWithProposedAdditionalControls;
    return (
      <Table isStriped isNarrow className="is-hoverable is-fullwidth">
        <thead>
        <tr>
          <th/>
          <th>Current lights + existing controls</th>
          <th>New lights + existing controls</th>
          <th>New lights + proposed additional controls</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td colSpan={4} style={style.table.title}>Load data and running hours</td>
        </tr>
        <tr>
          <td>Max load</td>
          <Cell value={current.maxLoad} suffix="kW"/>
          <Cell value={existing.maxLoad} suffix="kW"/>
          <Cell value={proposed.maxLoad} suffix="kW"/>
        </tr>
        <tr>
          <td>Occupancy</td>
          <Cell value={current.hoursPerAnnum} suffix="hrs/year"/>
          <Cell value={existing.hoursPerAnnum} suffix="hrs/year"/>
          <Cell value={proposed.hoursPerAnnum} suffix="hrs/year"/>
        </tr>
        <tr>
          <td>Running</td>
          <Cell value={current.runningHours} prefix="~" suffix="hrs/year"/>
          <Cell value={existing.runningHours} prefix="~" suffix="hrs/year"/>
          <Cell value={proposed.runningHours} prefix="~" suffix="hrs/year"/>
        </tr>
        <tr>
          <td>Consumption</td>
          <Cell value={current.consumption} suffix="kWh/year"/>
          <Cell value={existing.consumption} suffix="kWh/year"/>
          <Cell value={proposed.consumption} suffix="kWh/year"/>
        </tr>
        <tr>
          <td colSpan={4} style={style.table.title}>Savings</td>
        </tr>
        <tr>
          <td>Reduction</td>
          <td/>
          <Cell value={existing.reduction} suffix="kWh/year"/>
          <Cell value={proposed.reduction} suffix="kWh/year"/>
        </tr>
        <tr>
          <td>Of current Usage</td>
          <td/>
          <Cell value={existing.percentageOfUsage} suffix="%"/>
          <Cell value={proposed.percentageOfUsage} suffix="%"/>
        </tr>
        <tr>
          <td>Electricity reduction</td>
          <td/>
          <Cell value={existing.electricityCostReduction} prefix="£" suffix="/year"/>
          <Cell value={proposed.electricityCostReduction} prefix="£" suffix="/year"/>
        </tr>
        <tr>
          <td>Maintenance reduction</td>
          <td/>
          <Cell value={existing.maintenanceCostReduction} prefix="£" suffix="/year"/>
          <Cell value={proposed.maintenanceCostReduction} prefix="£" suffix="/year"/>
        </tr>
        <tr>
          <td>Total savings</td>
          <td/>
          <Cell value={existing.totalSaving} prefix="£" suffix="/year"/>
          <Cell value={proposed.totalSaving} prefix="£" suffix="/year"/>
        </tr>
        <tr>
          <td colSpan={4} style={style.table.title}>Cost and Payback time</td>
        </tr>
        <tr>
          <td>Upgrade cost</td>
          <td/>
          <Cell value={existing.costOfUpgrade} prefix="~£" suffix=" (excl. VAT)"/>
          <Cell value={proposed.costOfUpgrade} prefix="~£" suffix=" (excl. VAT)"/>
        </tr>
        <tr>
          <td>Payback time</td>
          <td/>
          <Cell value={existing.paybackTime} suffix=" years"/>
          <Cell value={proposed.paybackTime} suffix=" years"/>
        </tr>
        {this.greenBusiness}
        <tr>
          <td colSpan={4} style={style.table.title}>Reduction in carbon footprint</td>
        </tr>
        <tr>
          <td>CO&#8322; reduction</td>
          <td/>
          <Cell value={existing.co2Reduction} suffix="tonnes/year"/>
          <Cell value={proposed.co2Reduction} suffix="tonnes/year"/>
        </tr>
        </tbody>
      </Table>
    );
  }
}

export class Results extends React.Component<Props> {
  render() {
    return (
      <div style={{position: 'absolute', top: '15.1em', width: '100%', bottom: '3.1em', overflowY: 'auto' as 'auto'}}>
        <CardContent>
          <SummaryOfSaving {...this.props}/>
        </CardContent>
      </div>
    );
  }
}
