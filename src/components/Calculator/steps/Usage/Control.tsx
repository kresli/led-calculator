import { types } from 'mobx-state-tree';
export const Store = types.model('ControlStore')
  .props({
    id: types.identifier(),
    label: types.string,
    costPerFitting: types.number,
    reduction: types.number,
  });