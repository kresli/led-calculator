import React from 'react';
import * as Control from './Control';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import Dropdown from '../../Dropdown';
import { types, getSnapshot, getType } from 'mobx-state-tree';
import uniqid from 'uniqid';
import {Button} from 'bloomer';

type ControlType = typeof Control.Store.Type;

const sharedLightProps = {
  id: types.identifier(),
  label: types.string,
  image: types.optional(types.string, ''),
  size: types.string,
  power: types.number,
  lifetime: types.number,
  maintenanceCost: types.number,
};

export const LedLightStore = types.model('LedLight')
  .props({
    ...sharedLightProps,
    type: types.literal('led'),
    purchaseCostPerUnit: types.number,
    installLabourCostPerUnit: types.number,
    emergencyPackCostPerUnit: types.number,
    emergencyPackInstallLabourCostPerUnit: types.number,
  });

export const ClassicLightStore = types.model('ClassicLight')
  .props({
    ...sharedLightProps,
    type: types.literal('classic'),
    count: types.optional(types.number, 1),
    maintenanceLabourCost: types.number,
    ballasLoss: types.number,
    lightReplacement: LedLightStore,
    control: types.optional(types.reference(Control.Store), 'none'),
    controlInUse: types.optional(types.boolean, false),
    occupancyHoursPerAnnum: types.optional(types.number, 1),
    electricityPrice: types.optional(types.number, 1),
    emergencyLightCount: types.optional(types.number, 0),
    co2Emissions: types.optional(types.number, 0),
  })
  .views( self => ({
    get active(): boolean {
      return self.count > 0;
    },
  }))
  .views( self => ({
    get effectiveLoad(): number {
      const load: number = (self.power / 1000) * (1 + self.ballasLoss) * self.count;
      return load;
    }
  }))
  .views( self => ({
    get annualLoadWithExistingControls(): number {
      return self.effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
    }
  }))
  .views( self => ({
    get annualUsage(): number {
      return self.effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
    }
  }))
  .views( self => ({
    get electricityCostPerYear(): number {
      return self.annualUsage * (self.electricityPrice / 100);
    }
  }))
  .views( self => ({
    get annualMaintenanceCost(): number {
      const {maintenanceCost, maintenanceLabourCost, occupancyHoursPerAnnum, control, lifetime, count} = self;
      return (maintenanceCost + maintenanceLabourCost) * ((occupancyHoursPerAnnum * (1 - control.reduction)) / lifetime) * count;
    }
  }))
  .views( self => ({
    get totalRunningCost(): number {
      const {electricityCostPerYear, annualMaintenanceCost} = self;
      return electricityCostPerYear + annualMaintenanceCost;
    }
  }))
  .views( self => ({
    get ledData(): {
      label: string,
      power: number,
      size: string,
      effectiveLoad: number,
      annualLoadWithExistingControls: number,
      annualElectricityCost: number,
      purchaseCostPerUnit: number,
      installLabourCostPerUnit: number,
      installCostPerUnit: number,
      emergencyPackCostPerUnit: number,
      emergencyPackInstallLabourCostPerUnit: number,
      upgradeCost: number,
      maintenanceCost: number,
      maintenanceLabourCost: number,
      lifetime: number,
      annualMaintenanceCost: number,
      co2Reduction: number,
      co2Emissions: number,
    } {
      const {
        label,
        power,
        size,
        purchaseCostPerUnit,
        installLabourCostPerUnit,
        emergencyPackCostPerUnit,
        emergencyPackInstallLabourCostPerUnit,
        maintenanceCost,
        lifetime,
      } = self.lightReplacement;
      const effectiveLoad = (power / 1000) * self.count;
      const annualLoadWithExistingControls = effectiveLoad * self.occupancyHoursPerAnnum * (1 - self.control.reduction);
      const annualElectricityCost = annualLoadWithExistingControls * (self.electricityPrice / 100);
      const installCostPerUnit = purchaseCostPerUnit + installLabourCostPerUnit;
      const upgradeCost = installCostPerUnit * self.count + (emergencyPackCostPerUnit + emergencyPackInstallLabourCostPerUnit) * self.emergencyLightCount;
      const annumnMaintenanceCost = (maintenanceCost + self.maintenanceLabourCost) * ((self.occupancyHoursPerAnnum * (1 - self.control.reduction)) / lifetime) * self.count;
      const co2Reduction = (self.annualUsage - annualLoadWithExistingControls) * self.co2Emissions / 1000;
      return {
        label,
        power,
        size,
        effectiveLoad,
        annualLoadWithExistingControls,
        annualElectricityCost,
        purchaseCostPerUnit,
        installLabourCostPerUnit,
        installCostPerUnit,
        emergencyPackCostPerUnit,
        emergencyPackInstallLabourCostPerUnit,
        upgradeCost,
        maintenanceCost,
        maintenanceLabourCost: self.maintenanceLabourCost,
        lifetime,
        annualMaintenanceCost: annumnMaintenanceCost,
        co2Reduction,
        co2Emissions: self.co2Emissions,
      }
    }
  }))
  .views( self => ({
    get classicData(): {
      count: number,
      size: string,
      electricityPrice: number,
      occupancyHoursPerAnnum: number,
      label: string,
      power: number,
      ballasLoss: number,
      controls: string
      controlsSaving: number
      effectiveLoad: number,
      annualUsage: number,
      electricityCostPerYear: number,
      maintenanceCostPerLight: number,
      miantenanceLabourCostPerLight: number,
      lifetime: number,
      annualMaintenanceCost: number,
      totalRunningCost: number,
    } {
      const {label, size, power, ballasLoss, control, effectiveLoad, annualUsage, electricityCostPerYear, maintenanceCost, lifetime, annualMaintenanceCost, electricityPrice, occupancyHoursPerAnnum, count, maintenanceLabourCost, totalRunningCost} = self;
      return {
        count,
        size,
        electricityPrice,
        occupancyHoursPerAnnum,
        label,
        power,
        ballasLoss,
        controls: control.label,
        controlsSaving: control.reduction,
        effectiveLoad,
        annualUsage,
        electricityCostPerYear,
        maintenanceCostPerLight: maintenanceCost,
        miantenanceLabourCostPerLight: maintenanceLabourCost,
        lifetime,
        annualMaintenanceCost,
        totalRunningCost,
      };
    }
  }))
  .views( self => ({
    get controlData(): {
      label: string,
      reduction: number,
      increaseCostOfUpgrade: number,
      revisedMaintenanceCost: number,
      annualLoadWithAdditionalControls: number,
      reducedAnnualElectricityCost: number,
      totalCo2Reduction: number
    } {
      const {ledData} = self;
      const {label, reduction, costPerFitting} = self.control;
      const increaseCostOfUpgrade = costPerFitting * self.count;
      const revisedMaintenanceCost = ledData.annualMaintenanceCost * (1 - reduction);
      const annualLoadWithAdditionalControls = ledData.annualLoadWithExistingControls * (1 - reduction);
      const reducedAnnualElectricityCost = annualLoadWithAdditionalControls * (self.electricityPrice / 100);
      const totalCo2Reduction = (self.annualUsage - annualLoadWithAdditionalControls) * self.co2Emissions / 1000;
      return {
        label,
        reduction,
        increaseCostOfUpgrade,
        revisedMaintenanceCost,
        annualLoadWithAdditionalControls,
        reducedAnnualElectricityCost,
        totalCo2Reduction
      };
    }
  }))
  .views( self => ({
    get data(): {
      classic: {
        count: number,
        size: string,
        electricityPrice: number,
        occupancyHoursPerAnnum: number,
        label: string,
        power: number,
        ballasLoss: number,
        controls: string
        controlsSaving: number
        effectiveLoad: number,
        annualUsage: number,
        electricityCostPerYear: number,
        maintenanceCostPerLight: number,
        miantenanceLabourCostPerLight: number,
        lifetime: number,
        annualMaintenanceCost: number,
        totalRunningCost: number
      },
      led: {
        label: string,
        power: number,
        size: string,
        effectiveLoad: number,
        annualLoadWithExistingControls: number,
        annualElectricityCost: number,
        purchaseCostPerUnit: number,
        installLabourCostPerUnit: number,
        installCostPerUnit: number,
        emergencyPackCostPerUnit: number,
        emergencyPackInstallLabourCostPerUnit: number,
        upgradeCost: number,
        maintenanceCost: number,
        maintenanceLabourCost: number,
        lifetime: number,
        annualMaintenanceCost: number,
        co2Reduction: number,
        co2Emissions: number,
      },
      control: {
        label: string,
        reduction: number,
        increaseCostOfUpgrade: number,
        revisedMaintenanceCost: number,
        annualLoadWithAdditionalControls: number,
        reducedAnnualElectricityCost: number,
        totalCo2Reduction: number
      }
    } {
      return {
        classic: self.classicData,
        led: self.ledData,
        control: self.controlData,
      };
    }
  }))
  .actions(self => {
    function setOccupancyHoursPerAnnun(hours: number) {
      self.occupancyHoursPerAnnum = hours;
    }
    function setElectricityPrice(price: number) {
      self.electricityPrice = price;
    }
    function setCount(count: number) {
      self.count = count;
    }
    function clone() {
      const snapshoot: LightStoreType = JSON.parse(JSON.stringify(getSnapshot(self)));
      snapshoot.id = `${snapshoot.id}_${uniqid}`;
      return getType(self).create(snapshoot);
    }
    function setControl(control: ControlType) {
      self.control = control;
    }
    function setControlInUse(inUse: boolean) {
      self.controlInUse = inUse;
    }
    function setCo2Emissions(emissions: number) {
      self.co2Emissions = emissions;
    }
    return {setCount, clone, setControl, setControlInUse, setOccupancyHoursPerAnnun, setElectricityPrice, setCo2Emissions};
  });

export type LightStoreType = typeof ClassicLightStore.Type;

@observer
export class Light extends React.Component<{
  store: LightStoreType,
  controls: ControlType[],
  onDelete: (light: LightStoreType) => void
}> {

  @observable showBreakdown: boolean = true;
  @observable breakdownVisible: boolean = false;

  @action.bound
  onSetCountInput(ev: React.FormEvent<HTMLInputElement>) {
    this.props.store.setCount(parseInt(ev.currentTarget.value, 10));
  }

  @action.bound
  delete() {
    this.props.onDelete(this.props.store);
  }

  @computed
  get breakdown(): JSX.Element | null {
    if( !this.showBreakdown ) return null;
    const breakdown = !this.breakdownVisible ? null : (
      <pre>
        {JSON.stringify(this.props.store.data, null, '  ')}
      </pre>
    ) ;
    return (
      <div style={{display: 'flex', flexDirection: 'column' as 'column'}}>
        <Button onClick={() => this.breakdownVisible = !this.breakdownVisible} className="is-text" style={{flex: 1}}>
          Breakdown
        </Button>
        {breakdown}
      </div>
    )
  }

  render() {
    const {controls, store} = this.props;
    const {label, size, power, count} = store;
    return (
      <article className="box media" style={{display: 'flex'}}>
        <figure className="media-left is-hidden-mobile">
          <p className="image is-64x64">
            <img src="https://bulma.io/images/placeholders/128x128.png"/>
          </p>
        </figure>
        <div className="media-content">
          <div className="content" style={{display: 'flex', flexWrap: 'wrap'}}>
            <span style={{display: 'inline-flex', justifyContent: 'space-between', flexWrap: 'wrap', marginRight: '1em', marginBottom: '1em'}}>
              <span style={{display: 'flex', flexDirection: 'row'}}>
                <div className="field has-addons" style={{display: 'inline-flex'}}>
                  <span className="control">
                    <a className="button is-static">
                      <strong>{label}</strong> <small style={{marginLeft: '0.5em'}}>{size}</small> <small style={{marginLeft: '0.5em'}}>{power}W</small>
                    </a>
                  </span>
                  <div className="control" style={{width: '4em'}}>
                    <input
                      className="input"
                      type="number"
                      placeholder="24"
                      min="1"
                      onInput={this.onSetCountInput}
                      onChange={this.onSetCountInput}
                      value={count}
                    />
                  </div>
                </div>
              </span>
            </span>
            <ControlSwitch
              controls={controls}
              lightStore={store}
            />
          </div>
          {this.breakdown}
        </div>
        <div className="media-right">
          <button className="delete" onClick={this.delete}/>
        </div>
      </article>
    );
  }

}

@observer
class ControlSwitch extends React.Component<{
  controls: ControlType[];
  lightStore: LightStoreType;
}> {

  @action.bound
  onChangeControl(id: string) {
    const {controls} = this.props;
    const current = controls.find( c => c.id === id );
    if (!current) return;
    this.props.lightStore.setControl(current);
  }

  @action.bound
  onChangeInUse() {
    const inUse = !this.props.lightStore.controlInUse;
    this.props.lightStore.setControlInUse(inUse);
  }

  @computed
  get dropdownItems(): {id: string, label: string}[] {
    return this.props.controls.map( c => ({
      id: c.id.toString(),
      label: c.label,
    }));
  }

  @computed
  get checkboxAlreadyInUse(): JSX.Element | null {
    const {controlInUse, control} = this.props.lightStore;
    if (control.id.toString() === 'none') return null;
    const title = 'If the control is already in use please tick the box, otherwise the price of the new control will be added to the final cost';
    return (
      <div className="field" onClick={this.onChangeInUse} style={{marginLeft: '1em'}}>
        <input type="checkbox" name="test" className="switch" checked={controlInUse} onChange={() => {}}/>
        <label title={title}>control already bought</label>
      </div>
    )
  }

  render() {
    const {control} = this.props.lightStore;
    return(
      <div style={{display: 'flex', alignItems: 'center', marginBottom: '1em'}}>
        <Dropdown
          items={this.dropdownItems}
          selected={control.id.toString()}
          label="Controls"
          onChange={this.onChangeControl}
        />
        {this.checkboxAlreadyInUse}
      </div>
    );
  }
}

