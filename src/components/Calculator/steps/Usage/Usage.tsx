import React from 'react';
import { observer } from 'mobx-react';
import { action, computed } from 'mobx';
import * as Light from './Light';
import * as Control from './Control';
import { CardContent } from 'bloomer';
import Dropdown from '../../Dropdown';
import { LightStoreType } from './Light';
import { types } from 'mobx-state-tree';

interface CurrentLightSummary {
  maxLoad: number;
  hoursPerAnnum: number;
  consumption: number;
  runningHours: number;
  electricityCost: number;
  maintenanceCost: number;
  totalRunningCost: number;
}

interface NewLightSummary {
  maxLoad: number;
  hoursPerAnnum: number;
  consumption: number;
  runningHours: number;
  electricityCost: number
  reduction: number;
  percentageOfUsage: number;
  electricityCostReduction: number;
  maintenanceCostReduction: number;
  maintenanceCost: number;
  totalSaving: number;
  costOfUpgrade: number;
  paybackTime: number;
  greenBusiness: boolean;
  greenBusinessContribution: number;
  greenBusinessReducedUpgradeCost: number;
  greenBusinessPaybackTime: number;
  co2Reduction: number;
}

export interface LightSummary {
  currentLightsWithExistingControls: CurrentLightSummary,
  newLightsWithExistingControls: NewLightSummary,
  newLightsWithProposedAdditionalControls: NewLightSummary,
}

export const Store = types.model('Usage')
  .props({
    label: types.string,
    controls: types.array(Control.Store),
    lights: types.optional( types.array(Light.ClassicLightStore), [] ),
    availableLights: types.array(Light.ClassicLightStore),
    occupancyHours: types.optional(types.number, 0),
    electricityPrice: types.optional(types.number, 0),
    co2Emissions: types.optional(types.number, 0),
    greenBusiness: types.optional(types.boolean, false),
  })
  .views( self => ({
    get currentLightsWithExistingControls(): CurrentLightSummary {
      const maxLoad = self.lights.reduce( (load, l) =>  { return load + l.data.classic.effectiveLoad}, 0);
      const hoursPerAnnum = self.occupancyHours;
      const consumption = self.lights.reduce( (load, l) =>  { return load + l.data.classic.annualUsage}, 0);
      const electricityCost = self.lights.reduce( (load, l) =>  { return load + l.data.classic.electricityCostPerYear}, 0);
      const maintenanceCost = self.lights.reduce( (load, l) =>  { return load + l.data.classic.annualMaintenanceCost}, 0);
      const totalRunningCost = self.lights.reduce( (load, l) =>  { return load + l.data.classic.totalRunningCost}, 0);
      const runningHours = consumption / maxLoad;
      return {
        maxLoad,
        hoursPerAnnum,
        runningHours,
        consumption,
        electricityCost,
        maintenanceCost,
        totalRunningCost,
      }
    }
  }))
  .views( self => ({
    get newLightsWithExistingControls(): NewLightSummary {
      const current = self.currentLightsWithExistingControls;
      const maxLoad = self.lights.reduce( (load, l) =>  { return load + l.data.led.effectiveLoad}, 0);
      const hoursPerAnnum = current.hoursPerAnnum;
      const consumption = self.lights.reduce( (load, l) =>  { return load + l.data.led.annualLoadWithExistingControls}, 0);
      const runningHours = consumption / maxLoad;


      const electricityCost =  self.lights.reduce( (load, l) =>  { return load + l.data.led.annualElectricityCost}, 0);
      const reduction = current.consumption - consumption;
      const maintenanceCost = self.lights.reduce( (load, l) =>  { return load + l.data.led.annualMaintenanceCost}, 0);
      const percentageOfUsage = reduction / current.consumption;
      const electricityCostReduction = current.electricityCost - electricityCost;
      const maintenanceCostReduction = current.maintenanceCost - maintenanceCost;
      const totalSaving = electricityCostReduction + maintenanceCostReduction;

      const costOfUpgrade = self.lights.reduce( (load, l) =>  { return load + l.data.led.upgradeCost}, 0);
      const paybackTime = costOfUpgrade / totalSaving;

      const greenBusinessContribution = (self.greenBusiness && costOfUpgrade > 5000) ? Math.min(5000, costOfUpgrade * 0.15 * Math.min(1, 5 / (costOfUpgrade / totalSaving))) : 0;
      const greenBusinessReducedUpgradeCost = self.greenBusiness ? costOfUpgrade - greenBusinessContribution : 0
      const greenBusinessPaybackTime = self.greenBusiness ? greenBusinessReducedUpgradeCost / totalSaving : 0;
      const co2Reduction = self.lights.reduce( (load, l) =>  { return load + l.data.led.co2Reduction}, 0);

      return {
        maxLoad,
        hoursPerAnnum,
        runningHours,
        consumption,
        reduction,
        percentageOfUsage,
        electricityCostReduction,
        maintenanceCostReduction,
        totalSaving,
        electricityCost,
        maintenanceCost,
        costOfUpgrade,
        paybackTime,
        greenBusiness: self.greenBusiness,
        greenBusinessContribution,
        greenBusinessReducedUpgradeCost,
        greenBusinessPaybackTime,
        co2Reduction,
      };
    }
  }))
  .views( self => ({
    get newLightsWithProposedAdditionalControls(): NewLightSummary {
      const current = self.currentLightsWithExistingControls;
      const newExisting = self.newLightsWithExistingControls;
      const maxLoad = self.lights.reduce( (load, l) =>  { return load + l.data.led.effectiveLoad}, 0);
      const hoursPerAnnum = self.newLightsWithExistingControls.hoursPerAnnum;
      const consumption = self.lights.reduce( (load, l) =>  { return load + l.data.control.annualLoadWithAdditionalControls}, 0);
      const runningHours = consumption / maxLoad;
      const electricityCost =  self.lights.reduce( (load, l) =>  { return load + l.data.control.reducedAnnualElectricityCost}, 0);
      const reduction = current.consumption - consumption;
      const maintenanceCost = self.lights.reduce( (load, l) =>  { return load + l.data.control.revisedMaintenanceCost}, 0);
      const percentageOfUsage = reduction / current.consumption;
      const electricityCostReduction = current.electricityCost - electricityCost;
      const maintenanceCostReduction = current.maintenanceCost - maintenanceCost;
      const totalSaving = electricityCostReduction + maintenanceCostReduction;
      const costOfUpgrade = self.lights.reduce( (load, l) =>  { return load + l.data.control.increaseCostOfUpgrade}, 0) + newExisting.costOfUpgrade;
      const paybackTime = costOfUpgrade / totalSaving;
      const greenBusinessContribution = (self.greenBusiness && costOfUpgrade > 5000) ? Math.min(5000, costOfUpgrade * 0.15 * Math.min(1, 5 / (costOfUpgrade / totalSaving))) : 0;
      const greenBusinessReducedUpgradeCost = self.greenBusiness ? costOfUpgrade - greenBusinessContribution : 0
      const greenBusinessPaybackTime = self.greenBusiness ? greenBusinessReducedUpgradeCost / totalSaving : 0;
      const co2Reduction = self.lights.reduce( (load, l) =>  { return load + l.data.control.totalCo2Reduction}, 0);
      return {
        maxLoad,
        hoursPerAnnum,
        runningHours,
        consumption,
        reduction,
        percentageOfUsage,
        electricityCostReduction,
        maintenanceCostReduction,
        totalSaving,
        electricityCost,
        maintenanceCost,
        costOfUpgrade,
        paybackTime,
        greenBusiness: self.greenBusiness,
        greenBusinessContribution,
        greenBusinessReducedUpgradeCost,
        greenBusinessPaybackTime,
        co2Reduction,
      };
    }
  }))
  .views( self => ({
    get summary(): LightSummary {
      const {newLightsWithExistingControls, currentLightsWithExistingControls, newLightsWithProposedAdditionalControls} = self;
      const data = {
        currentLightsWithExistingControls: JSON.parse(JSON.stringify(currentLightsWithExistingControls)),
        newLightsWithExistingControls: JSON.parse(JSON.stringify(newLightsWithExistingControls)),
        newLightsWithProposedAdditionalControls: JSON.parse(JSON.stringify(newLightsWithProposedAdditionalControls))
      }
      return data;
    }
  }))
  .actions( self => {
    function addLight(light: LightStoreType | string) {
      if ( typeof light === 'string') {
          const sourceLight = self.availableLights.find( l => l.id === light );
          if ( !sourceLight ) return;
          light = sourceLight.clone() as LightStoreType;
      }
      light.setOccupancyHoursPerAnnun(self.occupancyHours);
      light.setElectricityPrice(self.electricityPrice);
      light.setCo2Emissions(self.co2Emissions);
      self.lights.unshift(light);
    }
    function removeLight(light: LightStoreType) {
      const index = self.lights.indexOf(light);
      self.lights.splice( index, 1 );
    }
    function setElectricityPrice(price: number) {
      self.electricityPrice = price;
      self.lights.forEach( l => l.setElectricityPrice(price) );
    }
    function setOccupancyHours(hours: number) {
      self.occupancyHours = hours;
      self.lights.forEach( l => l.setOccupancyHoursPerAnnun(hours));
    }
    function setCo2Emissions(emissions: number) {
      self.co2Emissions = emissions;
      self.lights.forEach( l => l.setCo2Emissions(emissions));
    }
    function setGreenBusiness( greenBusiness: boolean ) {
      self.greenBusiness = greenBusiness;
    }
    return {addLight, removeLight, setElectricityPrice, setOccupancyHours, setCo2Emissions, setGreenBusiness};
  })

interface Props {
  store: typeof Store.Type;
}

@observer
export class Usage extends React.Component<Props> {

  @action.bound
  addLight(light: LightStoreType) {
    this.props.store.addLight(light);
  }

  @computed
  get lights(): JSX.Element[] {
    return this.props.store.lights.map( (light) => (
      <Light.Light
        key={light.id.toString()}
        store={light}
        controls={this.props.store.controls}
        onDelete={this.props.store.removeLight}
      />
    ));
  }

  render() {
    const {availableLights} = this.props.store;
    return (
      <div style={{position: 'absolute', top: '15.1em', width: '100%', bottom: '3.1em', overflowY: 'auto' as 'auto'}}>
        <CardContent style={{background: '#f7f7f7', overflowY: 'auto' as 'auto', height: '100%'}}>
          <LightPicker available={availableLights} onAddLight={this.addLight}/>
          <div style={{marginTop: '1em'}}>
            {this.lights}
          </div>
        </CardContent>
      </div>
    );
  }
}

class LightPicker extends React.Component<{available: Light.LightStoreType[], onAddLight: (light: LightStoreType) => void}> {

  @action.bound
  onAddLight(id: string) {
    const {available} = this.props;
    const lightStore = available.find( l => l.id.toString() === id );
    if ( !lightStore ) return;
    this.props.onAddLight(lightStore.clone() as LightStoreType);
  }

  @computed
  get items(): {id: string, label: string}[] {
    return this.props.available.map( ({id, label, power, size}) => ({
      id: id.toString(),
      label: `${label} ${power}W ${size}`
    }) );
  }

  render() {
    return <Dropdown items={this.items} type="picker" separator label="Add light" onChange={this.onAddLight} maxHeight={240} fullWidth/>;
  }
}
