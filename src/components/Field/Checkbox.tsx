import React from 'react';
import { action, computed } from 'mobx';
import { observer } from 'mobx-react';
import { Help } from 'bloomer';
import marked from 'marked';
import { types } from 'mobx-state-tree';

interface Props {
  store: typeof Store.Type;
  onChange?: (checked: boolean) => void;
}

export const Store = types
  .model({
    id: types.identifier(),
    label: types.union(types.string, types.undefined),
    helpText: types.union(types.string, types.undefined),
    markdown: types.optional(types.boolean, false),
    checked: types.boolean,
  })
  .actions( self => {
    function toggleChecked() {
      self.checked = !self.checked;
    }
    return {toggleChecked};
  });

@observer
export class Checkbox extends React.Component<Props> {

  @action.bound
  toggleChecked() {
    this.props.store.toggleChecked();
    this.props.onChange && this.props.onChange(this.props.store.checked);
  }

  @computed
  get helpText(): JSX.Element | null {
    const {helpText, markdown} = this.props.store;
    if (!helpText) return null;
    return markdown && <Help dangerouslySetInnerHTML={{__html: marked(helpText)}}/> || <Help>{helpText}</Help>;
  }

  render() {
    const {label, checked} = this.props.store;
    return (
      <div className="field" onClick={this.toggleChecked}>
        <input type="checkbox" name="test" className="switch" checked={checked} onChange={() => {}}/>
        <label>{label}</label>
        {this.helpText}
      </div>
    );
  }
}