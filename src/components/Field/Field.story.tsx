import * as storybook from '@storybook/react';
import * as React from 'react';
import * as Field from './Field';
import { withKnobs } from '@storybook/addon-knobs';
import { Message, MessageBody, MessageHeader } from 'bloomer';
import { observer } from 'mobx-react';
import { action, observable } from 'mobx';
import { boolean, text, select, array, number } from '@storybook/addon-knobs';

const stories = storybook
  .storiesOf('Field', module)
  .addDecorator(withKnobs);

function storyPadding(story: {}) {
  return (
    <div style={{padding: '12px'}}>{story}</div>
  );
}

function makeCheckboxKnobs(knobsShared: {}) {
  return {
    ...knobsShared,
    ...{
      value: boolean('value?(checkbox)', false),
      defaultValue: boolean('defaultValue?(checkbox)', false),
    }
  }
}

function makeInputKnobs(knobsShared: {}) {
    return {
      ...knobsShared,
      ...{
        value: text('value?', ''),
        suffix: text('suffix?', ''),
        defaultValue: text('defaultValue?', ''),
        placeholder: text('placeholder?', ''),
        mandatory: boolean('mandatory?', false),
        leftIcon: text('leftIcon?', ''),
        rightIcon: text('rightIcon?', ''),
        minLength: number('minLength?', 0),
        maxLength: number('minLength?', 0),
        format: select('format?', ['string', 'number'], 'string'),
      }
    };
};

stories.add('playground', () => {
  const knobsShared: {[prop: string]: string | string[] | boolean} = {
    id: text('id', 'inputId'),
    type: select('type', { 'input': 'input', 'checkbox': 'checkbox' }, 'input') as string,
    helpText: text('helpText?', ''),
    showIfChecked: array('showIfChecked?', []),
    label: text('label?', ''),
    markdown: boolean('markdown?', false),
  };
  const knobsTypes: { [type: string]: (shared: {}) => {} } = {
    input: makeInputKnobs,
    checkbox: makeCheckboxKnobs,
  }

  const store = Field.Store.create(knobsTypes[knobsShared.type as string](knobsShared));

  return storyPadding(
    <Field.Field store={store}/>
  );
});

stories.add('showcase', () => {

  @observer
  class FieldStory extends React.Component<{store: typeof Field.Store.Type}> {

    @observable storeToJSON = '';

    @action.bound
    onInput() {
      const {store} = this.props;
      this.storeToJSON = JSON.stringify(store, null, '  ');
    }

    render() {
      const {store} = this.props;
      return (
        <div>
          <Field.Field store={Field.Store.create(store)}/>
          <Message>
            <MessageHeader>
              <p>store</p>
            </MessageHeader>
            <MessageBody>
              <pre>
                {JSON.stringify(store, null, '  ')}
              </pre>
            </MessageBody>
          </Message>
        </div>
      );
    }
  }

  function makeFieldStore(data: {}) {
    const defaults = {
      id: undefined,
      type: undefined,
      value: undefined,
      placeholder: undefined,
      mandatory: undefined,
      label: undefined,
      showIfChecked: [],
      suffix: undefined,
      helpText: undefined,
      leftIcon: undefined,
      rightIcon: undefined,
      maxLength: undefined,
      minLength: undefined,
      format: undefined,
      markdown: undefined,
    };
    return Field.Store.create({...defaults, ...data});
  }
  function createField(label: string, data: typeof Field.Store.Type )  {
    return (
      <div>
        <br/>
        <h3 className="title has-text-centered">{label}</h3>
        <FieldStory store={Field.Store.create(data)}/>
      </div>
    );
  }

  return (
    <div style={{padding: '12px'}}>
      {createField('simple', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
      }))}
      {createField('placeholder', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        placeholder: 'Placeholder'
      }))}
      {createField('mandatory', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        mandatory: true,
      }))}
      {createField('label', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        label: 'Label',
      }))}
      {createField('help text', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        helpText: 'Help Text',
      }))}
      {createField('default value', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        defaultValue: 'Default Value'
      }))}
      {createField('left icon', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        leftIcon: 'home'
      }))}
      {createField('right icon', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '',
        rightIcon: 'home',
      }))}
      {createField('value', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: 'Value',
      }))}
      {createField('min char length 3', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '12',
        minLength: 3,
      }))}
      {createField('max char length 3', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '1234',
        maxLength: 3,
      }))}
      {createField('format string', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: '123',
        format: 'string',
      }))}
      {createField('format number', makeFieldStore({
        id: 'myInput',
        type: 'input',
        value: 'string',
        format: 'number'
      }))}


      {createField('simple checkbox', makeFieldStore({
        id: 'myCheckbox',
        type: 'checkbox',
        value: true,
      }))}
      {createField('labeled checkbox',makeFieldStore({
        id: 'myCheckbox',
        type: 'checkbox',
        value: true,
        label: 'Builder'
      }))}
    </div>
  );
});
