import React from 'react';
import { observer } from 'mobx-react';
import { types } from 'mobx-state-tree';
import { observe } from 'mobx';
import * as Input from './Input';
import * as Checkbox from './Checkbox';
import * as Stepper from './Stepper';
import uniqid from 'uniqid';

export interface Props {
  store: typeof Store.Type;
}

export interface Event {
  value: string;
  valid: boolean;
  id: string;
}

export interface ExportData {
  value: boolean | string | number | undefined;
  label: string | undefined;
  id: string;
  mandatory: boolean | undefined;
  type: 'input' | 'checkbox' | 'stepper';
  format: 'string' | 'number' | undefined;
}

@observer
export class Field extends React.Component<Props> {

  render() {
    const {type, typeStore} = this.props.store;
    switch (type) {
      case 'input': return typeStore && <Input.Input store={typeStore as typeof Input.Store.Type}/> || <div>shite</div>;
      case 'checkbox': return <Checkbox.Checkbox store={typeStore as typeof Checkbox.Store.Type}/>;
      case 'stepper': return <Stepper.Stepper/>;
      default: return null;
    }
  }
}

const typeStores = new Map();

export const Store = types
  .model('Field', {
    id: types.optional(types.identifier(types.string), uniqid()),
    type: types.enumeration(['checkbox', 'input', 'stepper']),
    value: types.union(types.boolean, types.string, types.number),
    placeholder: types.optional(types.union(types.string, types.undefined), undefined),
    mandatory: types.optional(types.boolean, false),
    label: types.optional(types.union(types.string, types.undefined), undefined),
    showIfChecked: types.optional(types.array(types.string), []),
    suffix: types.optional(types.union(types.string, types.undefined), undefined),
    helpText: types.optional(types.union(types.string, types.undefined), undefined),
    leftIcon: types.optional(types.union(types.string, types.undefined), undefined),
    rightIcon: types.optional(types.union(types.string, types.undefined), undefined),
    maxLength: types.optional(types.union(types.number, types.undefined), undefined),
    minLength: types.optional(types.union(types.number, types.undefined), undefined),
    format: types.optional(types.union(types.enumeration(['number', 'string']), types.undefined), undefined),
    markdown: types.optional(types.boolean, false),
  })
  .views( self => ({
        get data(): ExportData {
          const {id, type, value, mandatory, format, label} = self;
          return { id, type, value, mandatory, format, label };
        },
        get typeStore(): typeof Input.Store.Type | typeof Checkbox.Store.Type {
          return typeStores.get(self);
        },
        get valid(): boolean {
          return typeStores.get(self).valid;
        }
      }))
  .actions( self => {
    function setValue(value: string | boolean) {
      self.value = value;
    }
    return {setValue};
  })
  .actions( self => {

    function beforeDestroy() {
      typeStores.delete(self);
    }

    function afterCreate() {
      const {
        id,
        type,
        value,
        placeholder,
        mandatory,
        label,
        suffix,
        helpText,
        leftIcon,
        rightIcon,
        maxLength,
        minLength,
        format,
        markdown,
      } = self;
      let typeStore;
      switch ( type ) {
        case 'input':
          typeStore = Input.Store.create({id, value, placeholder, mandatory, label, suffix, helpText, leftIcon, rightIcon, maxLength, minLength, format, markdown });
          observe(typeStore, 'value', change =>  self.setValue(change.newValue.storedValue));
          break;
        case 'checkbox':
          typeStore = Checkbox.Store.create({id, markdown, checked: value, label, helpText });
          observe(typeStore, 'checked', change =>  self.setValue(change.newValue.storedValue));
          break;
        case 'stepper':
          typeStore = Stepper.Store.create( {value} );
          observe(typeStore, 'value', change =>  self.setValue(change.newValue.storedValue));
          break;
        default: throw (`${self.type} is not valid type for Field component`);
      }
      typeStores.set(self, typeStore);
    }

    return {afterCreate, beforeDestroy};
  });

