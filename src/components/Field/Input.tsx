import React from 'react';
import {Input as BloomerInput, Field as BloomerField, Label, Control, Icon, Help, Button } from 'bloomer';
import { observer } from 'mobx-react';
import marked from 'marked';
import { types } from 'mobx-state-tree';

interface Props {
  store: typeof Store.Type;
}

export class Input extends React.Component<Props> {

  render() {
    const {store} = this.props;
    const {leftIcon, rightIcon, suffix, label, helpText, markdown} = store;

    return (
      <BloomerField hasAddons={true} style={{flexDirection: 'column'}}>
        <InputLabel label={label}/>
        <div style={{flexDirection: 'row', display: 'flex'}}>
          <Control hasIcons={(leftIcon || rightIcon) && true || false} isExpanded={true}>
            <InputArea store={store}/>
            <InputIcon icon={leftIcon} isAlign="left"/>
            <InputIcon icon={rightIcon} isAlign="right"/>
            <Required store={this.props.store}/>
          </Control>
          <Suffix suffix={suffix}/>
        </div>
        <HelpText helpText={helpText} markdown={markdown}/>
      </BloomerField>
    );
  }

}

export const Store = types
  .model('Input', {
    id: types.identifier(),
    label: types.union( types.string, types.undefined),
    placeholder: types.union( types.string, types.undefined),
    helpText: types.union( types.string, types.undefined),
    minLength: types.optional( types.number, 0 ),
    leftIcon: types.union( types.string, types.undefined),
    rightIcon: types.union( types.string, types.undefined),
    suffix: types.union( types.string, types.undefined),
    value: types.string,
    maxLength: types.union( types.number, types.undefined ),
    format: types.optional(types.enumeration(['number', 'string']), 'string'),
    mandatory: types.optional(types.boolean, false),
    markdown: types.boolean,
  })
  .views( self => ({
    get errorMessage(): string | null {
      const {value, format, mandatory} = self;
      if ( mandatory && self.value.length === 0 ) return 'This field is required';
      switch (format) {
        case 'number': return /^[0-9]*$/.test(value) ? null : 'Expected value to be number';
        case 'string': return /.*/.test(value) ? null : 'Expected value to be text';
        default: return `Format ${format} is not registered`;
      }
    },
    get valid(): boolean {
      return !this.errorMessage;
    }
  }))
  .actions( self => {
    function setValue(value: string) {
      self.value = value;
    }
    return {setValue};
  });

const InputArea: React.ClassicComponentClass<Props> = observer(({store}) => {
  const {valid, placeholder, value} = store;
  const color = valid ? undefined : 'danger';
  return (
    <BloomerInput
      isColor={color}
      placeholder={placeholder}
      value={value}
      onInput={(ev: React.FormEvent<HTMLInputElement>) => store.setValue(ev.currentTarget.value)}
      onChange={() => {}}
    />
  );
});

const HelpText: React.ClassicComponentClass<{
  helpText: string | undefined, markdown: boolean | undefined
}> = observer(({helpText, markdown}) => {
  if (!helpText) return null;
  if ( markdown ) {
    return <Control> <Help dangerouslySetInnerHTML={{__html: marked(helpText) }}/> </Control>;
  } else {
    return <Help>{helpText}</Help>;
  }
});

const InputLabel: React.ClassicComponentClass<{label: string | undefined}> = observer(({label}) => {
  return label && <Label>{label}</Label> || null;
});

const Suffix: React.SFC<{suffix: string | undefined}> = ({suffix}) => {
  if ( !suffix ) return null;
  return (
    <Control>
      <Button isStatic={true}>
        {suffix}
      </Button>
    </Control>
  );
};

const Required: React.ClassicComponentClass<{store: typeof Store.Type}> = observer(({store}) => {
  const {valid, errorMessage} = store;
  if ( valid ) return null;
  return (
    <div>
      {<Help isColor="danger">{errorMessage}</Help>}
    </div>
  );
});

const InputIcon: React.ClassicComponentClass<{
  icon: string | undefined,
  isAlign: 'left' | 'right'
}> = observer(props => {
  return props.icon && <Icon isSize="small" {...props}/> || null;
});

