import { Store } from './Store';
import * as Field from '../Field';

export interface Props {
  store: typeof Store.Type;
  onInput?: ( event: Field.Event ) => void;
}

export interface Data {
  fields: typeof Field.Store.Type[];
  data?: ExportData;
  valid?: boolean;
}

export interface ExportData {
  fields: Field.ExportData[];
}