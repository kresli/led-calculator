import * as storybook from '@storybook/react';
import * as React from 'react';
import * as Form from './';
import { withKnobs } from '@storybook/addon-knobs';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Message, MessageHeader, MessageBody } from 'bloomer';

const stories = storybook
  .storiesOf('Form', module)
  .addDecorator(withKnobs);

stories.add('data', () => {

  @observer
  class Output extends React.Component<{store: typeof Form.Store.Type}> {

    render() {
      return (
        <div style={{padding: '12px'}}>
          <Form.Form store={this.props.store}/>
          <br/>
          <Message>
            <MessageHeader>
              <p>data</p>
            </MessageHeader>
            <MessageBody>
              <pre>
                {JSON.stringify(toJS(this.props.store.data), null, '  ')}
              </pre>
            </MessageBody>
          </Message>
        </div>
      );
    }
  }

  const fields = [
    {
      id: 'Simple input',
      type: 'input' as 'input',
      value: '',
      label: 'Name'
    },
    {
      id: 'showMandatoryInput',
      type: 'checkbox' as 'checkbox',
      value: false,
      label: 'Show mandatory Input'
    },
    {
      id: 'hiddenMandatoryInput',
      label: 'Mandatory hidden input',
      type: 'input' as 'input',
      value: '',
      showIfChecked: ['showMandatoryInput'],
    }
  ];

  const store = Form.Store.create({fields});

  return <Output store={store}/>;
});