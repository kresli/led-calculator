import React from 'react';
import * as Field from '..//Field';
import { computed, action } from 'mobx';
import { observer } from 'mobx-react';
import { Props } from './Data';

@observer
export class Form extends React.Component<Props> {

  @action.bound
  onInput(ev: Field.Event) {
    // this.props.store.onInput();
    console.log(ev)
    if (this.props.onInput) this.props.onInput( ev );
  }

  @computed
  get fields(): (JSX.Element|null)[]{
    return this.props.store.visibleFields.map( (store: typeof Field.Store.Type) => (
      <Field.Field key={store.id} store={store}/>
    ));
  }

  render() {
    return <div>{this.fields}</div>;
  }
}
