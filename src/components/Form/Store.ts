import { types } from 'mobx-state-tree';
import * as Field from '../Field';
import { Data, ExportData } from './Data';

export const Store = types
  .model<Data>('Form', {
    fields: types.optional(types.array(Field.Store), []),
  })
  .views( self => ({
    get visibleFields(): typeof Field.Store.Type[]{
      return self.fields.filter( field => {
        if (!field.showIfChecked || field.showIfChecked.length === 0) return field;
        return field.showIfChecked.every( fieldId => {
          const checkField = self.fields.find(f => f.id === fieldId);
          if ( !checkField || checkField.type !== 'checkbox' ) return true;
          return checkField.value && true || false;
        });
      });
    },
  }))
  .views( self => ({
    get validationFields(): typeof Field.Store.Type[] {
      return self.visibleFields.filter( field => field.valid !== undefined );
    }
  }))
  .views(self => ({
    get valid(): boolean{
      return self.validationFields.every( field => field.valid === true );
    },
  }))
  .views( self => ({
    get data(): ExportData {
      return {
        fields: self.visibleFields.map( (field: typeof Field.Store.Type) => {
          const {value, label, id, mandatory, type, format} = field.data;
          return { value, label, id, mandatory, type, format };
        })
      };
    }
  }));
