import * as React from 'react';
import classNames from 'classnames';
import { action, computed } from 'mobx';
import { observer } from 'mobx-react';
import { types } from 'mobx-state-tree';

export type Props = {
  store: typeof Store.Type;
  onClick: (store: typeof Store.Type) => void;
};

export interface Data {
  id: string;
  label: string;
  active?: boolean;
  disabled?: boolean;
}

export const Store = types
  .model<Data>('StepperStep', {
    id: types.identifier(types.string),
    label: types.string,
    active: types.optional(types.boolean, false),
    disabled: types.optional(types.boolean, false)
  })
  .actions(self => {
    function setActive(active: boolean) {
      self.active = active;
    }
    function setDisabled(disabled: boolean) {
      self.disabled = disabled;
    }
    return {setActive, setDisabled};
  });

@observer
export class Step extends React.Component<Props> {

  @computed
  get className(): string {
    return classNames({
      'steps-segment': true,
      'is-active': this.props.store.active,
    });
  }

  @action.bound
  onClick() {
    this.props.onClick(this.props.store);
  }

  @computed
  get marker(): JSX.Element {
    const props = {
      className: 'steps-marker',
      onClick: this.props.store.disabled ? undefined : this.onClick,
    };
    return <a {...props}/>;
  }

  render() {
    return (
      <li className={this.className}>
        {this.marker}
        <div className="steps-content">
          <p className="is-size-7">{this.props.store.label}</p>
        </div>
      </li>
    );
  }
}