import * as storybook from '@storybook/react';
import React from 'react';
import * as Stepper from './Stepper';

const stories = storybook.storiesOf('Stepper', module);

function storyPadding(story: React.ReactNode) {
  return (
    <div style={{padding: '12px'}}>{story}</div>
  );
}

stories.add('default', () => {
  const store = Stepper.Store.create({
    steps: [
      {
        id: 'first',
        label: 'First',
      },
      {
        id: 'second',
        label: 'second',
      },
      {
        id: 'third',
        label: 'Third',
      },
      {
        id: 'fourth',
        label: 'Fourth',
      }
    ],
  });
  return storyPadding( <Stepper.Stepper store={store}/> );
});

stories.add('third step selected', () => {
  const store = Stepper.Store.create({
    steps: [
      {
        id: 'first',
        label: 'First',
      },
      {
        id: 'second',
        label: 'second',
      },
      {
        id: 'third',
        label: 'Third',
      },
      {
        id: 'fourth',
        label: 'Fourth',
      }
    ]
  });
  return storyPadding( <Stepper.Stepper store={store}/> );
});