import * as React from 'react';
import { observer } from 'mobx-react';
import { action, computed } from 'mobx';
import * as Step from './Step';
import { types } from 'mobx-state-tree';
import uniqid from 'uniqid'
;
export interface Data {
  steps: typeof Step.Store.Type[];
  id: string;
}

export interface Props {
  store: typeof Store.Type;
}

export const Store = types
  .model<Data>('Stepper', {
    id: types.optional( types.identifier(), uniqid() ),
    steps: types.array(Step.Store),
  })
  .views( self => ({
    get activeStep(): typeof Step.Store.Type {
      return self.steps.find(s => s.active === true) || self.steps[0];
    },
    get activeStepIndex(): number {
      return self.steps.indexOf( this.activeStep );
    },
    get activeStepId(): string {
      return this.activeStep.id;
    }
  }))
  .actions( self => {
    function setActiveStep(step: typeof Step.Store.Type | string) {
      let activeStep: typeof Step.Store.Type | undefined;
      if (typeof step === 'string') {
        activeStep = self.steps.find( s => s.id === step );
        if (!activeStep) return;
      } else {
        activeStep = step;
      }
      self.steps.forEach( s => s.setActive(false) );
      activeStep.active = true;
    }
    function nextStep() {
      const stepIndex = self.activeStepIndex + 1;
      if (stepIndex > self.steps.length - 1) return;
      self.steps.forEach( step => step.active = false );
      self.steps[stepIndex].active = true;
    }
    function previousStep() {
      const stepIndex = self.activeStepIndex - 1
      if (stepIndex < 0) return;
      self.steps.forEach( step => step.active = false );
      self.steps[stepIndex].active = true;
    }
    function disableAfterActiveStep(disabled: boolean) {
      for ( let index = self.activeStepIndex + 1; index < self.steps.length; index++) {
        self.steps[index].setDisabled(disabled)
      }
    }
    return {setActiveStep, nextStep, previousStep, disableAfterActiveStep};
  });

@observer
export class Stepper extends React.Component<Props> {

  @action.bound onClick(step: typeof Step.Store.Type) {
    this.props.store.setActiveStep(step);
  }

  @computed
  get steps(): React.ReactNode {
    return this.props.store.steps.map( (step, index) => {
      return (
        <Step.Step key={step.id} store={step} onClick={this.onClick} />
      );
    });
  }

  render() {
    return(
      <ul className="steps is-horizontal has-content-centered" style={{marginTop: '1em'}}>
        {this.steps}
      </ul>
    );
  }
}