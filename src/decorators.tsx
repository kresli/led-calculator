import React from 'react';
import { IntlProvider } from 'react-intl';
import { observer } from 'mobx-react';
import hoistNonReactStatic from 'hoist-non-react-statics';

export type Locale = 'en' | 'fr' | 'nl' | 'de' | 'es' | 'it' | 'en-us';
export type RocketPublicComponent<P> = React.ComponentClass<P> & {rocketName: string};

/* tslint:disable:no-any */
export function publicComponent<P extends {locale: Locale}>(Comp: RocketPublicComponent<P>): any {
  class PublicComponent extends React.Component<P> {

    render() {
      return (
        <IntlProvider locale={this.props.locale}>
            <Comp {...this.props}/>
        </IntlProvider>
      );
    }
  }
  hoistNonReactStatic(PublicComponent, Comp);
  return observer(PublicComponent);
}

/*tslint:disable:no-any */
export function ui<P> (Comp: React.ComponentClass<P>): any {
  class UiComponent extends React.Component<P> {
    render() {
      return (
        <div className="rocket">
          <Comp {...this.props}/>
        </div>
        );
    }
  }
  hoistNonReactStatic(UiComponent, Comp);
  return observer(UiComponent);
}
