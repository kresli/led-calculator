declare module 'react-icons/lib/md' {
    export * from 'react-icons/md';
}
declare module 'react-icons/lib/fa' {
    export * from 'react-icons/fa';
}
declare module 'react-icons/lib/ti' {
    export * from 'react-icons/ti';
}
declare module 'react-icons/lib/go' {
    export * from 'react-icons/go';
}
