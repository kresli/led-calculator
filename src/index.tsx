import './index.css';
import * as Calculator from './components/Calculator/Calculator';
import React from 'react';
import ReactDOM from 'react-dom';

declare global {
  interface Window { calculatorData: typeof Calculator.Store.Type }
}
console.log('script loaded')
document.addEventListener(
  'DOMContentLoaded',
  function() {

    const roots = document.querySelectorAll(`div[rocket-component*="Calculator"]`);
    console.log('domContentLoaded', roots);


    Array.from(roots).forEach( root => {
      const snapshot = window.calculatorData as typeof Calculator.Store.Type;
      const store = Calculator.Store.create(snapshot)
      ReactDOM.render((
          <Calculator.Calculator store={store}/>
        ),
        root
      );
    });

  });